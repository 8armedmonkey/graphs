package com.eightam.android.graphsdemo.widget;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.eightam.android.graphs.model.GraphType;
import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.widget.GraphView;
import com.eightam.android.graphs.widget.GraphViewFactory;
import com.eightam.android.graphsdemo.R;
import com.eightam.android.graphsdemo.util.InputMethodUtils;
import com.eightam.android.graphsdemo.util.ViewUtils;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GraphDemoAdapter extends PagerAdapter {

    public static final int PAGE_COUNT = 7;

    public static final int DEMO_INPUT = 0;
    public static final int DEMO_BUBBLE_GRAPH = 1;
    public static final int DEMO_MODERN_GRAPH = 2;
    public static final int DEMO_SOUND_GRAPH = 3;
    public static final int DEMO_CLASSIC_GRAPH = 4;
    public static final int DEMO_WAVE_GRAPH = 5;
    public static final int DEMO_INFINITE_GRAPH = 6;

    private static final int NUM_OF_VALUES = 5;

    private Context mContext;
    private float[] mValues;
    private SparseArray<SoftReference<View>> mViewRefs;
    private Listener mListener;

    public GraphDemoAdapter(Context context, Listener listener) {
        this(context, null, listener);
    }

    public GraphDemoAdapter(Context context, float[] values, Listener listener) {
        mContext = context;
        mViewRefs = new SparseArray<>();
        mListener = listener;

        initializeValues(values);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view;

        switch (position) {
            case DEMO_INPUT: {
                view = createDemoInputView(container);
                break;
            }

            case DEMO_BUBBLE_GRAPH: {
                view = createDemoGraphView(GraphType.BUBBLES);
                break;
            }

            case DEMO_MODERN_GRAPH: {
                view = createDemoGraphView(GraphType.MODERN);
                break;
            }

            case DEMO_SOUND_GRAPH: {
                view = createDemoGraphView(GraphType.SOUND);
                break;
            }

            case DEMO_CLASSIC_GRAPH: {
                view = createDemoGraphView(GraphType.CLASSIC);
                break;
            }

            case DEMO_WAVE_GRAPH: {
                view = createDemoGraphView(GraphType.WAVE);
                break;
            }

            case DEMO_INFINITE_GRAPH: {
                view = createDemoGraphView(GraphType.INFINITE);
                break;
            }

            default: {
                view = new View(mContext);
                break;
            }
        }

        if (view != null) {
            view.setBackgroundColor(mContext.getResources().getColor(android.R.color.black));

            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            container.addView(view, params);

            mViewRefs.put(position, new SoftReference<>(view));
        }
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mViewRefs.remove(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case DEMO_INPUT: {
                return mContext.getResources().getString(R.string.demo_input);
            }

            case DEMO_BUBBLE_GRAPH: {
                return mContext.getResources().getString(R.string.demo_bubble_graph);
            }

            case DEMO_MODERN_GRAPH: {
                return mContext.getResources().getString(R.string.demo_modern_graph);
            }

            case DEMO_SOUND_GRAPH: {
                return mContext.getResources().getString(R.string.demo_sound_graph);
            }

            case DEMO_CLASSIC_GRAPH: {
                return mContext.getResources().getString(R.string.demo_classic_graph);
            }

            case DEMO_WAVE_GRAPH: {
                return mContext.getResources().getString(R.string.demo_wave_graph);
            }

            case DEMO_INFINITE_GRAPH: {
                return mContext.getResources().getString(R.string.demo_infinite_graph);
            }
        }
        return super.getPageTitle(position);
    }

    public float[] getValues() {
        return mValues;
    }

    public void setValues(float[] values) {
        initializeValues(values);

        for (int i = 0, n = mViewRefs.size(); i < n; i++) {
            View view = mViewRefs.valueAt(i).get();

            if (view != null && view instanceof GraphView) {
                ((GraphView) view).setPoints(createDemoPoints());
                view.postInvalidate();
            }
        }
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    private void initializeValues(float[] values) {
        mValues = new float[NUM_OF_VALUES];
        Arrays.fill(mValues, 0.0f);

        if (values != null) {
            System.arraycopy(values, 0, mValues, 0, Math.min(values.length, mValues.length));
        }
    }

    private List<GraphPoint> createDemoPoints() {
        List<GraphPoint> points = new ArrayList<>();

        for (int i = 0, n = mValues.length; i < n; i++) {
            points.add(new GraphPoint(mValues[i]));
        }
        return points;
    }

    private View createDemoInputView(ViewGroup container) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.view_demo_input, container, false);

        ((EditText) view.findViewById(R.id.edit_value_1)).setText(String.valueOf(mValues[0]));
        ((EditText) view.findViewById(R.id.edit_value_2)).setText(String.valueOf(mValues[1]));
        ((EditText) view.findViewById(R.id.edit_value_3)).setText(String.valueOf(mValues[2]));
        ((EditText) view.findViewById(R.id.edit_value_4)).setText(String.valueOf(mValues[3]));
        ((EditText) view.findViewById(R.id.edit_value_5)).setText(String.valueOf(mValues[4]));

        view.findViewById(R.id.button_submit).setOnClickListener(new InputListener(GraphDemoAdapter.this, view));

        return view;
    }

    private View createDemoGraphView(GraphType graphType) {
        View view = GraphViewFactory.createGraphView(mContext, graphType, createDemoPoints());
        view.setOnClickListener(new GraphListener((GraphView) view));

        return view;
    }

    public interface Listener {

        void onNewValuesSet(float[] newValues);

    }

    private static class InputListener implements View.OnClickListener {

        GraphDemoAdapter mGraphDemoAdapter;
        View mInputView;

        public InputListener(GraphDemoAdapter demoAdapter, View inputView) {
            mGraphDemoAdapter = demoAdapter;
            mInputView = inputView;
        }

        @Override
        public void onClick(View v) {
            float[] values = new float[NUM_OF_VALUES];
            Arrays.fill(values, 0.0f);

            try {
                values[0] = Float.parseFloat(((EditText) mInputView.findViewById(R.id.edit_value_1)).getText().toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            try {
                values[1] = Float.parseFloat(((EditText) mInputView.findViewById(R.id.edit_value_2)).getText().toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            try {
                values[2] = Float.parseFloat(((EditText) mInputView.findViewById(R.id.edit_value_3)).getText().toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            try {
                values[3] = Float.parseFloat(((EditText) mInputView.findViewById(R.id.edit_value_4)).getText().toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            try {
                values[4] = Float.parseFloat(((EditText) mInputView.findViewById(R.id.edit_value_5)).getText().toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            hideSoftInputFromWindow();

            mGraphDemoAdapter.setValues(values);

            if (mGraphDemoAdapter.mListener != null) {
                mGraphDemoAdapter.mListener.onNewValuesSet(values);
            }
        }

        void hideSoftInputFromWindow() {
            List<EditText> editTexts = ViewUtils.findViewsByClass(mInputView, EditText.class);

            if (editTexts != null) {
                for (EditText editText : editTexts) {
                    if (InputMethodUtils.hideSoftInputFromWindow(editText)) {
                        break;
                    }
                }
            }
        }

    }

    private static class GraphListener implements View.OnClickListener {

        GraphView mGraphView;

        public GraphListener(GraphView graphView) {
            mGraphView = graphView;
        }

        @Override
        public void onClick(View v) {
            mGraphView.startAnimation();
        }
    }

}
