package com.eightam.android.graphsdemo.util;

import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class ViewUtils {

    public static <T extends View> List<T> findViewsByClass(View rootView, Class<T> clazz) {
        List<T> views = new ArrayList<>();

        if (rootView instanceof ViewGroup) {
            ViewGroup rootViewGroup = (ViewGroup) rootView;

            for (int i = 0, n = rootViewGroup.getChildCount(); i < n; i++) {
                views.addAll(findViewsByClass(rootViewGroup.getChildAt(i), clazz));
            }
        } else if (clazz.isInstance(rootView)) {
            views.add((T) rootView);
        }
        return views;
    }

}
