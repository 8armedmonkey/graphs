package com.eightam.android.graphsdemo.util;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class InputMethodUtils {

    public static boolean hideSoftInputFromWindow(View view) {
        if (view != null) {
            Context context = view.getContext();

            if (context != null) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                return imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
        return false;
    }

}
