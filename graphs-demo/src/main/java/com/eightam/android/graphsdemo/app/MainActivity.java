package com.eightam.android.graphsdemo.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.eightam.android.graphsdemo.R;
import com.eightam.android.graphsdemo.fragment.GraphDemoFragment;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class MainActivity extends AppCompatActivity {

    private static final String FRAGMENT_GRAPH_DEMO = "graphDemo";

    @InjectView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.inject(MainActivity.this);

        setSupportActionBar(mToolbar);

        if (savedInstanceState == null) {
            showGraphDemo();
        }
    }

    private void showGraphDemo() {
        GraphDemoFragment graphDemoFragment =
                (GraphDemoFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_GRAPH_DEMO);

        if (graphDemoFragment == null) {
            graphDemoFragment = GraphDemoFragment.newInstance();

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, graphDemoFragment, FRAGMENT_GRAPH_DEMO)
                    .commit();
        }
    }

}
