package com.eightam.android.graphsdemo.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eightam.android.graphsdemo.R;
import com.eightam.android.graphsdemo.widget.GraphDemoAdapter;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class GraphDemoFragment extends Fragment implements GraphDemoAdapter.Listener {

    float[] mValues = new float[]{10.0f, -20.0f, -30.0f, 20.0f, 10.0f};

    @InjectView(R.id.view_pager)
    ViewPager mViewPager;

    OnPageChangeListener mOnPageChangeListener;

    public static GraphDemoFragment newInstance() {
        return new GraphDemoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_graph_demo, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(GraphDemoFragment.this, view);

        mOnPageChangeListener = new OnPageChangeListener();

        mViewPager.setAdapter(new GraphDemoAdapter(getActivity(), mValues, GraphDemoFragment.this));
        mViewPager.addOnPageChangeListener(mOnPageChangeListener);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mValues = ((GraphDemoAdapter) mViewPager.getAdapter()).getValues();
        mViewPager.removeOnPageChangeListener(mOnPageChangeListener);
    }

    @Override
    public void onNewValuesSet(float[] newValues) {
        if (mViewPager != null) {
            int nextItem = Math.min(mViewPager.getAdapter().getCount() - 1, mViewPager.getCurrentItem() + 1);
            mViewPager.setCurrentItem(nextItem);
        }
    }

    private class OnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {

        @Override
        public void onPageSelected(int position) {
            Activity activity = getActivity();

            if (activity != null && activity instanceof AppCompatActivity) {
                ActionBar actionBar = ((AppCompatActivity) activity).getSupportActionBar();

                if (actionBar != null) {
                    actionBar.setTitle(mViewPager.getAdapter().getPageTitle(position));
                }
            }
        }

    }

}
