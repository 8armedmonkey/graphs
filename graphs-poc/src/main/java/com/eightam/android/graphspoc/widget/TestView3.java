package com.eightam.android.graphspoc.widget;

import android.animation.Animator;
import android.animation.FloatEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.eightam.android.graphs.widget.HasAnimation;

import java.util.ArrayList;
import java.util.List;

public class TestView3 extends View implements HasAnimation {

    private static final long ANIMATION_DURATION_MILLIS = 800;

    private List<Rectangle> mRectangles;
    private Paint mPaint;
    private Animator mAnimator;
    private float mScaleX;
    private float mScaleY;

    public TestView3(Context context) {
        super(context);
        initialize();
    }

    public TestView3(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public TestView3(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @Override
    public void startAnimation() {
        synchronized (TestView3.this) {
            stopAnimation();

            mScaleX = 1.0f;
            mScaleY = 0.0f;

            mAnimator = ValueAnimator.ofObject(new ScaleYAnimator(), 0.0f, 1.0f);
            mAnimator.setInterpolator(new OvershootInterpolator(2.0f));
            mAnimator.setDuration(ANIMATION_DURATION_MILLIS);
            mAnimator.start();
        }
    }

    @Override
    public void stopAnimation() {
        synchronized (TestView3.this) {
            if (mAnimator != null) {
                mAnimator.end();
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();

        float viewWidth = getWidth();
        float viewHeight = getHeight();
        float shortestWidth = Math.min(viewWidth, viewHeight);

        canvas.translate(0.5f * (viewWidth - shortestWidth), 0.5f * (viewHeight - shortestWidth));
        canvas.scale(mScaleX, mScaleY, 0, 0.5f * (viewHeight - shortestWidth));

        for (Rectangle rectangle : mRectangles) {
            drawRectangle(canvas, rectangle);
        }

        canvas.restore();
    }

    private void initialize() {
        mRectangles = new ArrayList<>();
        mRectangles.add(new Rectangle(0.05f, 0.85f, 0.15f, 0.95f, randomColor()));
        mRectangles.add(new Rectangle(0.20f, 0.70f, 0.35f, 0.85f, randomColor()));
        mRectangles.add(new Rectangle(0.40f, 0.55f, 0.50f, 0.65f, randomColor()));
        mRectangles.add(new Rectangle(0.55f, 0.35f, 0.70f, 0.50f, randomColor()));
        mRectangles.add(new Rectangle(0.75f, 0.10f, 0.95f, 0.30f, randomColor()));

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(Color.RED);

        mScaleX = 1.0f;
        mScaleY = 1.0f;
    }

    private int randomColor() {
        int a = (int) (0.75f * 255);
        int r = (int) ((Math.random() * 128) + 64);
        int g = (int) ((Math.random() * 128) + 64);
        int b = (int) ((Math.random() * 128) + 64);

        return Color.argb(a, r, g, b);
    }

    private void drawRectangle(Canvas canvas, Rectangle rectangle) {
        canvas.save();

        float shortestWidth = Math.min(getWidth(), getHeight());

        float left = rectangle.mLeft * shortestWidth;
        float top = rectangle.mTop * shortestWidth;
        float right = rectangle.mRight * shortestWidth;
        float bottom = rectangle.mBottom * shortestWidth;

        canvas.scale(rectangle.mScaleX, rectangle.mScaleY, 0.5f * (right + left), 0.5f * (bottom + top));

        mPaint.setColor(rectangle.mColor);
        canvas.drawRect(left, top, right, bottom, mPaint);

        canvas.restore();
    }

    private static class Rectangle {

        float mLeft;
        float mTop;
        float mRight;
        float mBottom;
        float mScaleX;
        float mScaleY;
        int mColor;

        public Rectangle(float left, float top, float right, float bottom, int color) {
            mLeft = left;
            mTop = top;
            mRight = right;
            mBottom = bottom;
            mScaleX = 1.0f;
            mScaleY = 1.0f;
            mColor = color;
        }

    }

    private class ScaleYAnimator extends FloatEvaluator {

        @NonNull
        @Override
        public Float evaluate(float fraction, Number startValue, Number endValue) {
            Float value = super.evaluate(fraction, startValue, endValue);
            mScaleY = value;
            postInvalidate();

            return value;
        }

    }

}
