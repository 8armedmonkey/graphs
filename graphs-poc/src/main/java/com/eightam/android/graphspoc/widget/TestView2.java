package com.eightam.android.graphspoc.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.FloatEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.eightam.android.graphs.widget.HasAnimation;

import java.util.ArrayList;
import java.util.List;

public class TestView2 extends View implements HasAnimation {

    private static final long ANIMATION_DURATION_MILLIS = 400;
    private static final long ANIMATION_START_DELAY_MODIFIER_MILLIS = 200;

    private List<Rectangle> mRectangles;
    private Paint mPaint;
    private Animator mAnimator;

    public TestView2(Context context) {
        super(context);
        initialize();
    }

    public TestView2(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public TestView2(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @Override
    public void startAnimation() {
        synchronized (TestView2.this) {
            stopAnimation();

            mAnimator = new AnimatorSet();
            List<Animator> animators = new ArrayList<>();

            for (int i = 0, n = mRectangles.size(); i < n; i++) {
                Rectangle rectangle = mRectangles.get(i);
                rectangle.mScaleX = 1.0f;
                rectangle.mScaleY = 1 / ((rectangle.mBottom - rectangle.mTop) * 100);

                Animator rectangleAnimator = ValueAnimator.ofObject(new RectangleScaleYAnimator(rectangle), 0.0f, 1.0f);
                rectangleAnimator.setInterpolator(new OvershootInterpolator(2.0f));
                rectangleAnimator.setStartDelay(i * ANIMATION_START_DELAY_MODIFIER_MILLIS);
                rectangleAnimator.setDuration(ANIMATION_DURATION_MILLIS);

                animators.add(rectangleAnimator);
            }

            ((AnimatorSet) mAnimator).playTogether(animators);
            mAnimator.start();
        }
    }

    @Override
    public void stopAnimation() {
        synchronized (TestView2.this) {
            if (mAnimator != null) {
                mAnimator.end();
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float viewWidth = getWidth();
        float viewHeight = getHeight();
        float shortestWidth = Math.min(viewWidth, viewHeight);

        canvas.translate(0.5f * (viewWidth - shortestWidth), 0.5f * (viewHeight - shortestWidth));

        for (Rectangle rectangle : mRectangles) {
            drawRectangle(canvas, rectangle);
        }
    }

    private void initialize() {
        mRectangles = new ArrayList<>();
        mRectangles.add(new Rectangle(0.10f, 0.30f, 0.11f, 0.70f, randomColor()));
        mRectangles.add(new Rectangle(0.30f, 0.25f, 0.31f, 0.75f, randomColor()));
        mRectangles.add(new Rectangle(0.50f, 0.40f, 0.51f, 0.60f, randomColor()));
        mRectangles.add(new Rectangle(0.70f, 0.20f, 0.71f, 0.80f, randomColor()));
        mRectangles.add(new Rectangle(0.90f, 0.35f, 0.91f, 0.65f, randomColor()));

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
    }

    private int randomColor() {
        int a = (int) (0.75f * 255);
        int r = (int) ((Math.random() * 128) + 64);
        int g = (int) ((Math.random() * 128) + 64);
        int b = (int) ((Math.random() * 128) + 64);

        return Color.argb(a, r, g, b);
    }

    private void drawRectangle(Canvas canvas, Rectangle rectangle) {
        canvas.save();

        float shortestWidth = Math.min(getWidth(), getHeight());

        float left = rectangle.mLeft * shortestWidth;
        float top = rectangle.mTop * shortestWidth;
        float right = rectangle.mRight * shortestWidth;
        float bottom = rectangle.mBottom * shortestWidth;

        canvas.scale(rectangle.mScaleX, rectangle.mScaleY, 0.5f * (right + left), 0.5f * (bottom + top));

        mPaint.setColor(rectangle.mColor);
        canvas.drawRect(left, top, right, bottom, mPaint);

        canvas.restore();
    }

    private static class Rectangle {

        float mLeft;
        float mTop;
        float mRight;
        float mBottom;
        float mScaleX;
        float mScaleY;
        int mColor;

        public Rectangle(float left, float top, float right, float bottom, int color) {
            mLeft = left;
            mTop = top;
            mRight = right;
            mBottom = bottom;
            mScaleX = 1.0f;
            mScaleY = 1.0f;
            mColor = color;
        }

    }

    private class RectangleScaleYAnimator extends FloatEvaluator {

        Rectangle mRectangle;

        public RectangleScaleYAnimator(Rectangle rectangle) {
            mRectangle = rectangle;
        }

        @NonNull
        @Override
        public Float evaluate(float fraction, Number startValue, Number endValue) {
            Float value = super.evaluate(fraction, startValue, endValue);
            mRectangle.mScaleY = value;
            postInvalidate();

            return value;
        }

    }

}
