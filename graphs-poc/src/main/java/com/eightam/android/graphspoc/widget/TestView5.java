package com.eightam.android.graphspoc.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.FloatEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.eightam.android.graphs.widget.HasAnimation;

import java.util.ArrayList;
import java.util.List;

public class TestView5 extends View implements HasAnimation {

    private static final long ANIMATION_DURATION_MILLIS = 400;
    private static final long ANIMATION_START_DELAY_MODIFIER_MILLIS = 50;

    private List<Triangle> mTriangles;
    private Paint mPaint;
    private Animator mAnimator;

    public TestView5(Context context) {
        super(context);
        initialize();
    }

    public TestView5(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public TestView5(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @Override
    public void startAnimation() {
        synchronized (TestView5.this) {
            stopAnimation();

            mAnimator = new AnimatorSet();
            List<Animator> animators = new ArrayList<>();

            for (int i = 0, n = mTriangles.size(); i < n; i++) {
                Triangle triangle = mTriangles.get(i);
                triangle.mScaleX = 1.0f;
                triangle.mScaleY = 0.0f;

                Animator animator = ValueAnimator.ofObject(new ScaleYAnimator(triangle), 0.0f, 1.0f);
                animator.setInterpolator(new OvershootInterpolator(2.0f));
                animator.setDuration(ANIMATION_DURATION_MILLIS);
                animator.setStartDelay(i * ANIMATION_START_DELAY_MODIFIER_MILLIS);

                animators.add(animator);
            }

            ((AnimatorSet) mAnimator).playTogether(animators);
            mAnimator.start();
        }
    }

    @Override
    public void stopAnimation() {
        synchronized (TestView5.this) {
            if (mAnimator != null) {
                mAnimator.end();
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();

        float viewWidth = getWidth();
        float viewHeight = getHeight();
        float shortestWidth = Math.min(viewWidth, viewHeight);

        canvas.translate(0.5f * (viewWidth - shortestWidth), 0.5f * (viewHeight - shortestWidth));
        canvas.rotate(-45.0f, 0.5f * shortestWidth, 0.5f * shortestWidth);

        for (Triangle triangle : mTriangles) {
            drawTriangle(canvas, triangle);
        }

        canvas.restore();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        for (Triangle triangle : mTriangles) {
            triangle.buildPath(right - left, bottom - top);
        }

        postInvalidate();
    }

    private void initialize() {
        mTriangles = new ArrayList<>();
        mTriangles.add(new Triangle(0.02f, 0.20f, 0.32f, 0.50f, randomColor()));
        mTriangles.add(new Triangle(0.30f, 0.40f, 0.40f, 0.50f, randomColor()));
        mTriangles.add(new Triangle(0.38f, 0.50f, 0.68f, 0.80f, randomColor()));
        mTriangles.add(new Triangle(0.66f, 0.50f, 0.76f, 0.60f, randomColor()));
        mTriangles.add(new Triangle(0.74f, 0.25f, 0.99f, 0.50f, randomColor()));

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    }

    private int randomColor() {
        int a = (int) (0.75f * 255);
        int r = (int) ((Math.random() * 128) + 64);
        int g = (int) ((Math.random() * 128) + 64);
        int b = (int) ((Math.random() * 128) + 64);

        return Color.argb(a, r, g, b);
    }

    private void drawTriangle(Canvas canvas, Triangle triangle) {
        canvas.save();

        float shortestWidth = Math.min(getWidth(), getHeight());

        float left = triangle.mLeft * shortestWidth;
        float right = triangle.mRight * shortestWidth;

        canvas.scale(triangle.mScaleX, triangle.mScaleY, 0.5f * (right + left), 0.5f * shortestWidth);

        if (triangle.mPath != null) {
            mPaint.setColor(triangle.mColor);
            canvas.drawPath(triangle.mPath, mPaint);
        }

        canvas.restore();
    }

    private static class Triangle {

        static final int DIRECTION_UP = 0;
        static final int DIRECTION_DOWN = 1;

        float mLeft;
        float mTop;
        float mRight;
        float mBottom;
        float mScaleX;
        float mScaleY;

        int mColor;
        int mDirection;
        Path mPath;

        public Triangle(float left, float top, float right, float bottom, int color) {
            mLeft = left;
            mTop = top;
            mRight = right;
            mBottom = bottom;
            mScaleX = 1.0f;
            mScaleY = 1.0f;

            mColor = color;
            mDirection = mBottom <= 0.5f ? DIRECTION_UP : DIRECTION_DOWN;
        }

        void buildPath(int containerWidth, int containerHeight) {
            float shortestWidth = Math.min(containerWidth, containerHeight);

            float pathLeft = mLeft * shortestWidth;
            float pathTop = mTop * shortestWidth;
            float pathRight = mRight * shortestWidth;
            float pathBottom = mBottom * shortestWidth;

            Path path = new Path();
            path.setFillType(Path.FillType.EVEN_ODD);

            switch (mDirection) {
                case DIRECTION_UP: {
                    path.moveTo(pathLeft, pathBottom);
                    path.lineTo(pathRight, pathBottom);
                    path.lineTo(0.5f * (pathLeft + pathRight), pathTop);
                    break;
                }

                case DIRECTION_DOWN: {
                    path.moveTo(pathLeft, pathTop);
                    path.lineTo(pathRight, pathTop);
                    path.lineTo(0.5f * (pathLeft + pathRight), pathBottom);
                    break;
                }
            }

            path.close();

            mPath = path;
        }

    }

    private class ScaleYAnimator extends FloatEvaluator {

        Triangle mTriangle;

        public ScaleYAnimator(Triangle triangle) {
            mTriangle = triangle;
        }

        @NonNull
        @Override
        public Float evaluate(float fraction, Number startValue, Number endValue) {
            Float value = super.evaluate(fraction, startValue, endValue);
            mTriangle.mScaleY = value;
            postInvalidate();

            return value;
        }
    }

}
