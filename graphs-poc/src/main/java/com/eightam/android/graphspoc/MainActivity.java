package com.eightam.android.graphspoc;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import com.eightam.android.graphs.widget.HasAnimation;
import com.eightam.android.graphspoc.widget.TestView1;
import com.eightam.android.graphspoc.widget.TestView2;
import com.eightam.android.graphspoc.widget.TestView3;
import com.eightam.android.graphspoc.widget.TestView4;
import com.eightam.android.graphspoc.widget.TestView5;
import com.eightam.android.graphspoc.widget.TestView6;
import com.eightam.android.graphspoc.widget.TestView7;
import com.eightam.android.graphspoc.widget.TestView8;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class MainActivity extends AppCompatActivity {

    @InjectView(R.id.view_pager)
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.inject(MainActivity.this);

        mViewPager.setAdapter(new Adapter(MainActivity.this));
    }

    private static class Adapter extends PagerAdapter {

        static final int PAGE_COUNT = 8;

        Context mContext;

        public Adapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = null;

            switch (position) {
                case 0:
                    view = new TestView1(mContext);
                    break;
                case 1:
                    view = new TestView2(mContext);
                    break;
                case 2:
                    view = new TestView3(mContext);
                    break;
                case 3:
                    view = new TestView4(mContext);
                    break;
                case 4:
                    view = new TestView5(mContext);
                    break;
                case 5:
                    view = new TestView6(mContext);
                    break;
                case 6:
                    view = new TestView7(mContext);
                    break;
                case 7:
                    view = new TestView8(mContext);
                    break;
            }

            if (view instanceof HasAnimation) {
                view.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        ((HasAnimation) v).startAnimation();
                    }

                });
            }

            if (view != null) {
                container.addView(view);
            }

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }

}
