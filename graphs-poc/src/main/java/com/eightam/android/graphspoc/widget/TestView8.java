package com.eightam.android.graphspoc.widget;

import android.animation.Animator;
import android.animation.FloatEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import com.eightam.android.graphs.widget.HasAnimation;

import java.util.ArrayList;
import java.util.List;

public class TestView8 extends View implements HasAnimation {

    private static final float MAX_SIZE = 4384.0f;
    private static final float GAP = 45.0f;

    private static final int COLOR_POSITIVE = 0xFF009285;
    private static final int COLOR_NEGATIVE = 0xFFF4C100;

    private static final long ANIMATION_DURATION_MILLIS = 400;

    private List<Float> mValues;
    private List<Paint> mPaints;
    private Path mPath;
    private float mRotationAngle;
    private Animator mAnimator;

    public TestView8(Context context) {
        super(context);
        initialize();
    }

    public TestView8(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public TestView8(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @Override
    public void startAnimation() {
        synchronized (TestView8.this) {
            stopAnimation();

            mAnimator = ValueAnimator.ofObject(new RotateAnimator(), 0.0f, 360.0f);
            mAnimator.setDuration(ANIMATION_DURATION_MILLIS);
            mAnimator.start();
        }
    }

    @Override
    public void stopAnimation() {
        synchronized (TestView8.this) {
            if (mAnimator != null) {
                mAnimator.end();
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();

        float viewWidth = getWidth();
        float viewHeight = getHeight();
        float shortestWidth = Math.min(viewWidth, viewHeight);

        canvas.translate(0.5f * (viewWidth - shortestWidth), 0.5f * (viewHeight - shortestWidth));

        drawSpiral(canvas);

        canvas.restore();
    }

    private void initialize() {
        mValues = new ArrayList<>();
        mValues.add(200.0f);
        mValues.add(-50.0f);
        mValues.add(100.0f);
        mValues.add(-20.0f);
        mValues.add(-10.0f);

        initializePaints();
        initializePath();
    }

    private void initializePaints() {
        mPaints = new ArrayList<>();

        int numOfValues = mValues.size();

        float firstAbsValue = Math.abs(mValues.get(0).floatValue());
        float minAbsValue = firstAbsValue;
        float maxAbsValue = firstAbsValue;
        float sumAbsValue = 0;

        for (Float value : mValues) {
            float absValue = Math.abs(value.floatValue());

            minAbsValue = Math.min(minAbsValue, absValue);
            maxAbsValue = Math.max(maxAbsValue, absValue);

            sumAbsValue += absValue;
        }

        float maxSizeWithoutGaps = MAX_SIZE - GAP * (numOfValues - 1);
        float spacing = 0;

        for (int i = 0; i < numOfValues; i++) {
            float value = mValues.get(i);
            float valueInterval = Math.abs(value) / sumAbsValue * maxSizeWithoutGaps;

            float[] intervals;

            if (spacing == 0) {
                intervals = new float[] { valueInterval, MAX_SIZE - valueInterval };
            } else {
                intervals = new float[] { 0.0f, spacing, valueInterval, MAX_SIZE - (spacing + valueInterval) };
            }

            PathEffect pathEffect = new DashPathEffect(intervals, 0.0f);

            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(Float.compare(value, 0.0f) > 0 ? COLOR_POSITIVE : COLOR_NEGATIVE);
            paint.setPathEffect(pathEffect);
            paint.setStrokeWidth(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8.0f, getResources().getDisplayMetrics()));
            paint.setStyle(Paint.Style.STROKE);

            mPaints.add(paint);

            spacing = spacing + valueInterval + GAP;
        }
    }

    private void initializePath() {
        mPath = new Path();

        mPath.moveTo(484.93f, 409.24f);
        mPath.cubicTo(484.93f, 457.77f, 445.62f, 497.11f, 397.13f, 497.11f);
        mPath.cubicTo(343.78f, 497.11f, 300.54f, 453.84f, 300.54f, 400.46f);
        mPath.cubicTo(300.54f, 341.74f, 348.1f, 294.13f, 406.78f, 294.13f);
        mPath.cubicTo(471.33f, 294.13f, 523.66f, 346.5f, 523.66f, 411.09f);
        mPath.cubicTo(523.66f, 482.14f, 466.1f, 539.73f, 395.1f, 539.73f);
        mPath.cubicTo(316.99f, 539.73f, 253.68f, 476.38f, 253.68f, 398.22f);
        mPath.cubicTo(253.68f, 312.25f, 323.33f, 242.56f, 409.24f, 242.56f);
        mPath.cubicTo(503.74f, 242.56f, 580.35f, 319.22f, 580.35f, 413.79f);
        mPath.cubicTo(580.35f, 517.81f, 496.08f, 602.14f, 392.13f, 602.14f);
        mPath.cubicTo(277.78f, 602.14f, 185.08f, 509.38f, 185.08f, 394.95f);
        mPath.cubicTo(185.08f, 269.08f, 287.05f, 167.05f, 412.83f, 167.05f);
        mPath.cubicTo(551.19f, 167.05f, 663.36f, 279.29f, 663.36f, 417.74f);
        mPath.cubicTo(663.36f, 570.05f, 539.98f, 693.51f, 387.78f, 693.51f);
        mPath.cubicTo(220.36f, 693.51f, 84.64f, 557.7f, 84.64f, 390.17f);
        mPath.cubicTo(84.64f, 205.88f, 233.93f, 56.49f, 418.09f, 56.49f);
    }

    private void drawSpiral(Canvas canvas) {
        canvas.save();

        float viewWidth = getWidth();
        float viewHeight = getHeight();
        float shortestWidth = Math.min(viewWidth, viewHeight);

        canvas.scale(shortestWidth / 750.0f, shortestWidth / 750.0f);
        canvas.rotate(mRotationAngle, 375.0f, 375.0f);

        for (Paint paint : mPaints) {
            canvas.drawPath(mPath, paint);
        }

        canvas.restore();
    }

    private class RotateAnimator extends FloatEvaluator {

        @NonNull
        @Override
        public Float evaluate(float fraction, Number startValue, Number endValue) {
            Float value = super.evaluate(fraction, startValue, endValue);
            mRotationAngle = value;
            postInvalidate();

            return value;
        }

    }

}
