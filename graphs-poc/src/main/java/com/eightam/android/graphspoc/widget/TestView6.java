package com.eightam.android.graphspoc.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.FloatEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.eightam.android.graphs.widget.HasAnimation;

import java.util.ArrayList;
import java.util.List;

public class TestView6 extends View implements HasAnimation {

    private static final long ANIMATION_DURATION_MILLIS = 400;
    private static final float OVAL_RADIUS = 0.02f;

    private List<Point> mPoints;
    private RectF mPointRect;
    private Paint mLinePaint;
    private Paint mPointPaint;
    private Animator mAnimator;

    public TestView6(Context context) {
        super(context);
        initialize();
    }

    public TestView6(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public TestView6(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @Override
    public void startAnimation() {
        synchronized (TestView6.this) {
            stopAnimation();

            mAnimator = new AnimatorSet();
            List<Animator> animators = new ArrayList<>();

            for (int i = 0, n = mPoints.size(); i < n; i++) {
                Point point = mPoints.get(i);

                /* If you need to animate also on the x-axis.
                point.mDisplayX = 0.5f;

                Animator animatorX = ValueAnimator.ofObject(new PointXAnimator(point), point.mDisplayX, point.mX);
                animatorX.setInterpolator(new OvershootInterpolator(2.0f));
                animatorX.setDuration(ANIMATION_DURATION_MILLIS);

                animators.add(animatorX);
                */

                point.mDisplayY = 0.5f;

                Animator animatorY = ValueAnimator.ofObject(new PointYAnimator(point), point.mDisplayY, point.mY);
                animatorY.setInterpolator(new OvershootInterpolator(2.0f));
                animatorY.setDuration(ANIMATION_DURATION_MILLIS);

                animators.add(animatorY);
            }

            ((AnimatorSet) mAnimator).playTogether(animators);
            mAnimator.start();
        }
    }

    @Override
    public void stopAnimation() {
        synchronized (TestView6.this) {
            if (mAnimator != null) {
                mAnimator.end();
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();

        float viewWidth = getWidth();
        float viewHeight = getHeight();
        float shortestWidth = Math.min(viewWidth, viewHeight);

        canvas.translate(0.5f * (viewWidth - shortestWidth), 0.5f * (viewHeight - shortestWidth));

        drawLines(canvas);
        drawPoints(canvas);

        canvas.restore();
    }

    private void initialize() {
        int fillColor = randomColor() | 0xFF000000;
        int strokeColor = randomColor();

        mPoints = new ArrayList<>();
        mPoints.add(new Point(0.05f, 0.20f, fillColor, strokeColor));
        mPoints.add(new Point(0.27f, 0.65f, fillColor, strokeColor));
        mPoints.add(new Point(0.49f, 0.70f, fillColor, strokeColor));
        mPoints.add(new Point(0.71f, 0.25f, fillColor, strokeColor));
        mPoints.add(new Point(0.93f, 0.25f, fillColor, strokeColor));

        mLinePaint = new Paint();
        mLinePaint.setAntiAlias(true);
        mLinePaint.setColor(strokeColor);
        mLinePaint.setStrokeWidth(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2.0f, getResources().getDisplayMetrics()));
        mLinePaint.setStyle(Paint.Style.STROKE);

        mPointPaint = new Paint();
        mPointPaint.setAntiAlias(true);
        mPointPaint.setStrokeWidth(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2.0f, getResources().getDisplayMetrics()));
        mPointPaint.setStyle(Paint.Style.FILL);

        mPointRect = new RectF();
    }

    private int randomColor() {
        int a = (int) (0.75f * 255);
        int r = (int) ((Math.random() * 128) + 64);
        int g = (int) ((Math.random() * 128) + 64);
        int b = (int) ((Math.random() * 128) + 64);

        return Color.argb(a, r, g, b);
    }

    private void drawLines(Canvas canvas) {
        float shortestWidth = Math.min(getWidth(), getHeight());

        for (int i = 0, n = mPoints.size() - 1; i < n; i++) {
            Point p1 = mPoints.get(i);
            Point p2 = mPoints.get(i + 1);

            float x1 = p1.mDisplayX * shortestWidth;
            float y1 = p1.mDisplayY * shortestWidth;
            float x2 = p2.mDisplayX * shortestWidth;
            float y2 = p2.mDisplayY * shortestWidth;

            canvas.save();
            canvas.drawLine(x1, y1, x2, y2, mLinePaint);
            canvas.restore();
        }
    }

    private void drawPoints(Canvas canvas) {
        float shortestWidth = Math.min(getWidth(), getHeight());

        for (int i = 0, n = mPoints.size(); i < n; i++) {
            Point p = mPoints.get(i);
            float x = p.mDisplayX * shortestWidth;
            float y = p.mDisplayY * shortestWidth;
            float r = OVAL_RADIUS * shortestWidth;

            float left = x - r;
            float top = y - r;
            float right = x + r;
            float bottom = y + r;

            mPointRect.set(left, top, right, bottom);

            canvas.save();

            mPointPaint.setStyle(Paint.Style.FILL);
            mPointPaint.setColor(p.mFillColor);
            canvas.drawOval(mPointRect, mPointPaint);

            mPointPaint.setStyle(Paint.Style.STROKE);
            mPointPaint.setColor(p.mStrokeColor);
            canvas.drawOval(mPointRect, mPointPaint);

            canvas.restore();
        }
    }

    private static class Point {

        float mX;
        float mY;
        float mDisplayX;
        float mDisplayY;
        int mFillColor;
        int mStrokeColor;

        public Point(float x, float y, int fillColor, int strokeColor) {
            mX = x;
            mY = y;
            mDisplayX = x;
            mDisplayY = y;
            mFillColor = fillColor;
            mStrokeColor = strokeColor;
        }

    }

    class PointXAnimator extends FloatEvaluator {

        Point mPoint;

        public PointXAnimator(Point point) {
            mPoint = point;
        }

        @NonNull
        @Override
        public Float evaluate(float fraction, Number startValue, Number endValue) {
            Float value = super.evaluate(fraction, startValue, endValue);
            mPoint.mDisplayX = value;
            postInvalidate();

            return value;
        }
    }

    class PointYAnimator extends FloatEvaluator {

        Point mPoint;

        public PointYAnimator(Point point) {
            mPoint = point;
        }

        @NonNull
        @Override
        public Float evaluate(float fraction, Number startValue, Number endValue) {
            Float value = super.evaluate(fraction, startValue, endValue);
            mPoint.mDisplayY = value;
            postInvalidate();

            return value;
        }
    }

}
