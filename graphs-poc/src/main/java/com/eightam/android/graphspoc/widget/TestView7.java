package com.eightam.android.graphspoc.widget;

import android.animation.Animator;
import android.animation.FloatEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.eightam.android.graphs.widget.HasAnimation;

import java.util.ArrayList;
import java.util.List;

public class TestView7 extends View implements HasAnimation {

    private static final long ANIMATION_DURATION_MILLIS = 400;

    private List<Point> mPoints;
    private Path mPath;
    private Paint mLinePaint;
    private Paint mShadowPaint;
    private int[] mGradientColors;
    private int[] mAlphaGradientColors;
    private float mScaleX;
    private float mScaleY;
    private Animator mAnimator;

    public TestView7(Context context) {
        super(context);
        initialize();
    }

    public TestView7(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public TestView7(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @Override
    public void startAnimation() {
        synchronized (TestView7.this) {
            stopAnimation();

            mScaleX = 1.0f;
            mScaleY = 0.0f;

            mAnimator = ValueAnimator.ofObject(new ScaleYAnimator(), 0.0f, 1.0f);
            mAnimator.setInterpolator(new OvershootInterpolator(2.0f));
            mAnimator.setDuration(ANIMATION_DURATION_MILLIS);
            mAnimator.start();
        }
    }

    @Override
    public void stopAnimation() {
        synchronized (TestView7.this) {
            if (mAnimator != null) {
                mAnimator.end();
            }
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        float viewWidth = getWidth();
        float viewHeight = getHeight();
        float shortestWidth = Math.min(viewWidth, viewHeight);

        // XXX Pretty strange, but x1 needs to start from 0.0f probably because of the canvas translation.
        float x1 = 0.0f;
        float x2 = shortestWidth;

        mLinePaint.setShader(new LinearGradient(x1, 0, x2, 0, mGradientColors, null, Shader.TileMode.CLAMP));
        mShadowPaint.setShader(new LinearGradient(x1, 0, x2, 0, mAlphaGradientColors, null, Shader.TileMode.REPEAT));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();

        float viewWidth = getWidth();
        float viewHeight = getHeight();
        float shortestWidth = Math.min(viewWidth, viewHeight);

        canvas.translate(0.5f * (viewWidth - shortestWidth), 0.5f * (viewHeight - shortestWidth));
        canvas.scale(mScaleX, mScaleY, 0, 0.5f * (viewHeight - shortestWidth));

        drawCurves(canvas);

        canvas.restore();
    }

    private void initialize() {
        mPoints = new ArrayList<>();
        mPoints.add(new Point(0.05f, 0.20f));
        mPoints.add(new Point(0.27f, 0.65f));
        mPoints.add(new Point(0.49f, 0.70f));
        mPoints.add(new Point(0.71f, 0.25f));
        mPoints.add(new Point(0.93f, 0.25f));

        mPath = new Path();

        mLinePaint = new Paint();
        mLinePaint.setAntiAlias(true);
        mLinePaint.setStrokeWidth(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10.0f, getResources().getDisplayMetrics()));
        mLinePaint.setStyle(Paint.Style.STROKE);
        mLinePaint.setStrokeCap(Paint.Cap.ROUND);

        mShadowPaint = new Paint();
        mShadowPaint.setAntiAlias(true);
        mShadowPaint.setStrokeWidth(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20.0f, getResources().getDisplayMetrics()));
        mShadowPaint.setStyle(Paint.Style.STROKE);
        mShadowPaint.setStrokeCap(Paint.Cap.ROUND);

        int alphaGradientColor1 = 0x05F4C100;
        int alphaGradientColor2 = 0x05009285;

        int gradientColor1 = alphaGradientColor1 | 0xFF000000;
        int gradientColor2 = alphaGradientColor2 | 0xFF000000;

        mGradientColors = new int[]{gradientColor1, gradientColor2, gradientColor1};
        mAlphaGradientColors = new int[]{alphaGradientColor1, alphaGradientColor2, alphaGradientColor2};

        mScaleX = 1.0f;
        mScaleY = 1.0f;
    }

    private int randomColor() {
        int a = (int) (0.02f * 255);
        int r = (int) ((Math.random() * 128) + 64);
        int g = (int) ((Math.random() * 128) + 64);
        int b = (int) ((Math.random() * 128) + 64);

        return Color.argb(a, r, g, b);
    }

    /**
     * Calculation of the Bezier control points:
     * http://stackoverflow.com/questions/8287949/android-how-to-draw-a-smooth-line-following-your-finger
     */
    private void drawCurves(Canvas canvas) {
        float shortestWidth = Math.min(getWidth(), getHeight());

        mPath.reset();

        int n = mPoints.size();

        if (n > 1) {
            for (int i = 0; i < n; i++) {
                Point point = mPoints.get(i);

                if (i == 0) {
                    Point next = mPoints.get(i + 1);
                    point.mDx = 0.33f * (next.mX - point.mX);
                    point.mDy = 0.33f * (next.mY - point.mY);

                } else if (i == n - 1) {
                    Point prev = mPoints.get(i - 1);
                    point.mDx = 0.33f * (point.mX - prev.mX);
                    point.mDy = 0.33f * (point.mY - prev.mY);

                } else {
                    Point next = mPoints.get(i + 1);
                    Point prev = mPoints.get(i - 1);
                    point.mDx = 0.33f * (next.mX - prev.mX);
                    point.mDy = 0.33f * (next.mY - prev.mY);
                }
            }
        }

        for (int i = 1; i < n; i++) {
            Point point = mPoints.get(i);
            Point prev = mPoints.get(i - 1);

            float cx1 = (prev.mX + prev.mDx) * shortestWidth;
            float cy1 = (prev.mY + prev.mDy) * shortestWidth;
            float cx2 = (point.mX - point.mDx) * shortestWidth;
            float cy2 = (point.mY - point.mDy) * shortestWidth;
            float x1 = prev.mX * shortestWidth;
            float y1 = prev.mY * shortestWidth;
            float x2 = point.mX * shortestWidth;
            float y2 = point.mY * shortestWidth;

            mPath.moveTo(x1, y1);
            mPath.cubicTo(cx1, cy1, cx2, cy2, x2, y2);
        }

        /*
        float shadowStrokeWidth = mShadowPaint.getStrokeWidth();

        for (int i = 0; i < 10; i++) {
            float ty = 0.4f + 0.1f * i;

            canvas.save();
            canvas.translate(0, ty * shadowStrokeWidth);
            canvas.drawPath(mPath, mShadowPaint);
            canvas.restore();
        }
        */

        canvas.save();
        canvas.drawPath(mPath, mLinePaint);
        canvas.restore();
    }

    private static class Point {

        float mX;
        float mY;
        float mDx;
        float mDy;

        public Point(float x, float y) {
            mX = x;
            mY = y;
            mDx = x;
            mDy = y;
        }

    }

    private class ScaleYAnimator extends FloatEvaluator {

        @NonNull
        @Override
        public Float evaluate(float fraction, Number startValue, Number endValue) {
            Float value = super.evaluate(fraction, startValue, endValue);
            mScaleY = value;
            postInvalidate();

            return value;
        }

    }

}
