package com.eightam.android.graphs.widget;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.animation.OvershootInterpolator;

import com.eightam.android.graphs.animation.ScaleYEvaluator;
import com.eightam.android.graphs.behavior.Scalable;
import com.eightam.android.graphs.layout.GraphLayoutManager;
import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.point.IndexableGraphPoint;
import com.eightam.android.graphs.renderer.BezierCurveRenderer;
import com.eightam.android.graphs.renderer.GraphLineRenderer;

import java.util.ArrayList;
import java.util.List;

public class WaveGraphView extends LineGraphView<IndexableGraphPoint> {

    private static final float OVERSHOOT_TENSION = 2.0f;

    private com.eightam.android.graphs.behavior.Scalable mScalable;
    private Animator mAnimator;

    public WaveGraphView(Context context) {
        super(context);
        initialize();
    }

    public WaveGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public WaveGraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @Override
    public void startAnimation() {
        synchronized (WaveGraphView.this) {
            mAnimator = ValueAnimator.ofObject(new ScaleYEvaluator(WaveGraphView.this, mScalable), 0.0f, 1.0f);

            if (mAnimationDurationMillis > 0) {
                mAnimator.setInterpolator(new OvershootInterpolator(OVERSHOOT_TENSION));
            }

            mAnimator.setDuration(mAnimationDurationMillis);
            mAnimator.start();
        }
    }

    @Override
    public void stopAnimation() {
        synchronized (WaveGraphView.this) {
            if (mAnimator != null) {
                mAnimator.end();
            }
        }
    }

    @Override
    public void setPoints(List<? extends GraphPoint> points) {
        super.setPoints(points);
        setPointsForGraphLineRenderer();
    }

    @Override
    public void setLayoutManager(GraphLayoutManager layoutManager) {
        super.setLayoutManager(layoutManager);
        setPointsForGraphLineRenderer();
    }

    @Override
    public void setGraphLineRenderer(GraphLineRenderer<IndexableGraphPoint> renderer) {
        if (!(renderer instanceof BezierCurveRenderer)) {
            throw new IllegalArgumentException("Incompatible line renderer: "
                    + WaveGraphView.class.getName() + " requires " + BezierCurveRenderer.class.getName());
        }

        super.setGraphLineRenderer(renderer);
        setPointsForGraphLineRenderer();
    }

    @Override
    protected void setPointsInternal(List<? extends GraphPoint> points) {
        List<IndexableGraphPoint> internalPoints = new ArrayList<>();
        int i = 0;

        for (GraphPoint point : points) {
            internalPoints.add(new IndexableGraphPoint(point, i++));
        }

        mPoints = internalPoints;
    }

    @Override
    protected void onPreRenderGraphPoints(Canvas canvas) {
        super.onPreRenderGraphPoints(canvas);

        int viewWidth = getWidth();
        int viewHeight = getHeight();
        int shortestWidth = Math.min(viewWidth, viewHeight);

        canvas.scale(mScalable.getScaleX(), mScalable.getScaleY(), 0.0f, 0.5f * shortestWidth);
    }

    private void initialize() {
        mScalable = new Scalable(1.0f, 0.0f);
    }

    private void setPointsForGraphLineRenderer() {
        if (mGraphLineRenderer != null) {
            ((BezierCurveRenderer) mGraphLineRenderer).setPoints(mPoints);
        }
    }

    public static class Builder extends LineGraphView.Builder<Builder, IndexableGraphPoint> {

        public Builder(Context context) {
            super(context);
        }

        @Override
        void createGraphView(Context context) {
            mGraphView = new WaveGraphView(context);
        }

    }

    private static class Scalable implements com.eightam.android.graphs.behavior.Scalable {

        float mScaleX;
        float mScaleY;

        public Scalable(float scaleX, float scaleY) {
            mScaleX = scaleX;
            mScaleY = scaleY;
        }

        @Override
        public float getScaleX() {
            return mScaleX;
        }

        @Override
        public void setScaleX(float scaleX) {
            mScaleX = scaleX;
        }

        @Override
        public float getScaleY() {
            return mScaleY;
        }

        @Override
        public void setScaleY(float scaleY) {
            mScaleY = scaleY;
        }
    }

}
