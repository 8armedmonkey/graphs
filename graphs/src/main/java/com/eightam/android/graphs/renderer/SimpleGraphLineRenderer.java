package com.eightam.android.graphs.renderer;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;

import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.point.TranslatableGraphPoint;

public class SimpleGraphLineRenderer implements GraphLineRenderer<TranslatableGraphPoint> {

    private Paint mPaint;

    public SimpleGraphLineRenderer() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    public void render(Canvas canvas, int containerWidth, int containerHeight, TranslatableGraphPoint p1, TranslatableGraphPoint p2) {
        canvas.save();

        int shortestWidth = Math.min(containerWidth, containerHeight);

        float x1 = (p1.getX() + p1.getTranslationX()) * shortestWidth;
        float y1 = (p1.getY() + p1.getTranslationY()) * shortestWidth;
        float x2 = (p2.getX() + p2.getTranslationX()) * shortestWidth;
        float y2 = (p2.getY() + p2.getTranslationY()) * shortestWidth;

        canvas.drawLine(x1, y1, x2, y2, mPaint);
        canvas.restore();
    }

    public void setStrokeWidth(float strokeWidth) {
        mPaint.setStrokeWidth(strokeWidth);
    }

    public void setStrokeColor(int color) {
        mPaint.setColor(color);
    }

    public void setShader(Shader shader) {
        mPaint.setShader(shader);
    }

}
