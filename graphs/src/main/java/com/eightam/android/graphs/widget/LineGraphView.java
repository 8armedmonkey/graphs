package com.eightam.android.graphs.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.renderer.GraphLineRenderer;
import com.eightam.android.graphs.renderer.GraphPointRenderer;

import java.util.List;

public abstract class LineGraphView<T extends GraphPoint> extends AutoCenteringGraphView<T> {

    protected GraphLineRenderer<T> mGraphLineRenderer;

    public LineGraphView(Context context) {
        super(context);
    }

    public LineGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LineGraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public GraphLineRenderer<T> getGraphLineRenderer() {
        return mGraphLineRenderer;
    }

    public void setGraphLineRenderer(GraphLineRenderer<T> renderer) {
        mGraphLineRenderer = renderer;
    }

    @Override
    protected void renderPoints(Canvas canvas, List<T> points, GraphPointRenderer<T> graphPointRenderer) {
        int viewWidth = getWidth();
        int viewHeight = getHeight();

        int n = points.size();

        if (mGraphLineRenderer != null) {
            GraphLineRenderer<T> graphLineRenderer = mGraphLineRenderer;

            for (int i = 0; i < n - 1; i++) {
                T p1 = points.get(i);
                T p2 = points.get(i + 1);

                graphLineRenderer.render(canvas, viewWidth, viewHeight, p1, p2);
            }
        }

        for (int i = 0; i < n; i++) {
            T point = points.get(i);
            graphPointRenderer.render(canvas, viewWidth, viewHeight, point);
        }
    }

    @Override
    protected void onPostRenderGraphPoints(Canvas canvas) {
    }

    public abstract static class Builder<B extends Builder, T extends GraphPoint> extends GraphView.Builder<B, T> {

        public Builder(Context context) {
            super(context);
        }

        public B withLineRenderer(GraphLineRenderer<T> lineRenderer) {
            ((LineGraphView<T>) mGraphView).setGraphLineRenderer(lineRenderer);
            return (B) Builder.this;
        }

    }

}
