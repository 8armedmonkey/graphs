package com.eightam.android.graphs.point;

public class GraphPoint {

    protected float mX;
    protected float mY;
    protected float mZ;

    public GraphPoint() {
        this(0.0f, 0.0f, 0.0f);
    }

    public GraphPoint(float z) {
        this(0.0f, 0.0f, z);
    }

    public GraphPoint(float x, float y, float z) {
        mX = x;
        mY = y;
        mZ = z;
    }

    public float getX() {
        return mX;
    }

    public void setX(float x) {
        mX = x;
    }

    public float getY() {
        return mY;
    }

    public void setY(float y) {
        mY = y;
    }

    public float getZ() {
        return mZ;
    }

    public void setZ(float z) {
        mZ = z;
    }

}
