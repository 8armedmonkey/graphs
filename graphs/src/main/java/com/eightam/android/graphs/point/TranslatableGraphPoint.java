package com.eightam.android.graphs.point;

import com.eightam.android.graphs.behavior.Translatable;

public class TranslatableGraphPoint extends GraphPoint implements Translatable {

    protected float mTranslationStartX;
    protected float mTranslationStartY;

    public TranslatableGraphPoint() {
        this(0.0f, 0.0f, 0.0f);
    }

    public TranslatableGraphPoint(float x, float y, float z) {
        this(x, y, z, 0.0f, 0.0f);
    }

    public TranslatableGraphPoint(float x, float y, float z, float translationStartX, float translationStartY) {
        super(x, y, z);
        mTranslationStartX = translationStartX;
        mTranslationStartY = translationStartY;
    }

    public TranslatableGraphPoint(GraphPoint point) {
        this(point.mX, point.mY, point.mZ);
    }

    public TranslatableGraphPoint(GraphPoint point, float translationStartX, float translationStartY) {
        this(point.mX, point.mY, point.mZ, translationStartX, translationStartY);
    }

    @Override
    public float getTranslationX() {
        return mTranslationStartX;
    }

    @Override
    public void setTranslationX(float translationX) {
        mTranslationStartX = translationX;
    }

    @Override
    public float getTranslationY() {
        return mTranslationStartY;
    }

    @Override
    public void setTranslationY(float translationY) {
        mTranslationStartY = translationY;
    }

}
