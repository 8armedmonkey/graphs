package com.eightam.android.graphs.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.renderer.GraphPointRenderer;

import java.util.List;

public abstract class SimpleGraphView<T extends GraphPoint> extends AutoCenteringGraphView<T> {

    public SimpleGraphView(Context context) {
        super(context);
    }

    public SimpleGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SimpleGraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void renderPoints(Canvas canvas, List<T> points, GraphPointRenderer<T> graphPointRenderer) {
        int viewWidth = getWidth();
        int viewHeight = getHeight();

        for (T point : points) {
            graphPointRenderer.render(canvas, viewWidth, viewHeight, point);
        }
    }

    @Override
    protected void onPostRenderGraphPoints(Canvas canvas) {
    }

}
