package com.eightam.android.graphs.renderer;

import android.graphics.Canvas;

import com.eightam.android.graphs.point.GraphPoint;

public interface GraphPointRenderer<T extends GraphPoint> {

    void render(Canvas canvas, int containerWidth, int containerHeight, T graphPoint);

}
