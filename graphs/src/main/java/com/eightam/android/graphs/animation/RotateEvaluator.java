package com.eightam.android.graphs.animation;

import com.eightam.android.graphs.behavior.Rotatable;
import com.eightam.android.graphs.widget.GraphView;

public class RotateEvaluator extends GraphAnimationEvaluator {

    private Rotatable mRotatable;

    public RotateEvaluator(GraphView graphView, Rotatable rotatable) {
        super(graphView);
        mRotatable = rotatable;
    }

    @Override
    protected void useValue(float value) {
        mRotatable.setRotationAngle(value);
    }

}
