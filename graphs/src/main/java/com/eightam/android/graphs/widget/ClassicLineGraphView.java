package com.eightam.android.graphs.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.OvershootInterpolator;

import com.eightam.android.graphs.animation.TranslateYEvaluator;
import com.eightam.android.graphs.layout.GraphLayoutManager;
import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.point.TranslatableGraphPoint;

import java.util.ArrayList;
import java.util.List;

public class ClassicLineGraphView extends LineGraphView<TranslatableGraphPoint> {

    private static final float OVERSHOOT_TENSION = 2.0f;

    private Animator mAnimator;

    public ClassicLineGraphView(Context context) {
        super(context);
    }

    public ClassicLineGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ClassicLineGraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void startAnimation() {
        synchronized (ClassicLineGraphView.this) {
            stopAnimation();

            mAnimator = new AnimatorSet();
            List<Animator> animators = new ArrayList<>();

            float paddingTop = mLayoutManager != null ? mLayoutManager.getRelativePaddingTop() : 0.0f;
            float paddingBottom = mLayoutManager != null ? mLayoutManager.getRelativePaddingBottom() : 0.0f;

            for (int i = 0, n = mPoints.size(); i < n; i++) {
                TranslatableGraphPoint point = mPoints.get(i);
                point.setTranslationX(0.0f);
                point.setTranslationY(-point.getY() + paddingTop + 0.5f * (1.0f - (paddingTop + paddingBottom)));

                Animator animator = ValueAnimator.ofObject(new TranslateYEvaluator(ClassicLineGraphView.this, point),
                        point.getTranslationY(), 0.0f);

                if (mAnimationDurationMillis > 0) {
                    animator.setInterpolator(new OvershootInterpolator(OVERSHOOT_TENSION));
                }

                animator.setDuration(mAnimationDurationMillis);

                animators.add(animator);
            }

            ((AnimatorSet) mAnimator).playTogether(animators);
            mAnimator.start();
        }
    }

    @Override
    public void stopAnimation() {
        synchronized (ClassicLineGraphView.this) {
            if (mAnimator != null) {
                mAnimator.end();
            }
        }
    }

    @Override
    public void setPoints(List<? extends GraphPoint> points) {
        super.setPoints(points);
        adjustPointTranslationYs();
    }

    @Override
    public void setLayoutManager(GraphLayoutManager layoutManager) {
        super.setLayoutManager(layoutManager);
        adjustPointTranslationYs();
    }

    @Override
    protected void setPointsInternal(List<? extends GraphPoint> points) {
        List<TranslatableGraphPoint> internalPoints = new ArrayList<>();

        for (GraphPoint point : points) {
            internalPoints.add(new TranslatableGraphPoint(point));
        }

        mPoints = internalPoints;
    }

    private void adjustPointTranslationYs() {
        if (mPoints != null) {
            float paddingTop = mLayoutManager != null ? mLayoutManager.getRelativePaddingTop() : 0.0f;
            float paddingBottom = mLayoutManager != null ? mLayoutManager.getRelativePaddingBottom() : 0.0f;

            for (TranslatableGraphPoint point : mPoints) {
                point.setTranslationX(0.0f);
                point.setTranslationY(-point.getY() + paddingTop + 0.5f * (1.0f - (paddingTop + paddingBottom)));
            }
        }
    }

    public static class Builder extends LineGraphView.Builder<Builder, TranslatableGraphPoint> {

        public Builder(Context context) {
            super(context);
        }

        @Override
        void createGraphView(Context context) {
            mGraphView = new ClassicLineGraphView(context);
        }

    }

}
