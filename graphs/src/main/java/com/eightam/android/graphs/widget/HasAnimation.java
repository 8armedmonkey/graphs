package com.eightam.android.graphs.widget;

public interface HasAnimation {

    void startAnimation();

    void stopAnimation();

}
