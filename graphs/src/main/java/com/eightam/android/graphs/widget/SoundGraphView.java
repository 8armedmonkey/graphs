package com.eightam.android.graphs.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.OvershootInterpolator;

import com.eightam.android.graphs.animation.ScaleYEvaluator;
import com.eightam.android.graphs.layout.GraphLayoutManager;
import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.point.ScalableGraphPoint;
import com.eightam.android.graphs.renderer.GraphPointRenderer;
import com.eightam.android.graphs.renderer.LineRenderer;

import java.util.ArrayList;
import java.util.List;

public class SoundGraphView extends SimpleGraphView<ScalableGraphPoint> {

    private static final long ANIMATION_START_DELAY = 200;
    private static final float OVERSHOOT_TENSION = 2.0f;

    private Animator mAnimator;

    public SoundGraphView(Context context) {
        super(context);
    }

    public SoundGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SoundGraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void startAnimation() {
        synchronized (SoundGraphView.this) {
            stopAnimation();

            float containerHeight = getHeight();
            float strokeWidth = 0.0f;
            float lineLengthModifier = 1.0f;

            if (mPointRenderer != null) {
                strokeWidth = ((LineRenderer) mPointRenderer).getStrokeWidth();
                lineLengthModifier = ((LineRenderer) mPointRenderer).getLineLengthModifier();
            }

            mAnimator = new AnimatorSet();
            List<Animator> animators = new ArrayList<>();

            for (int i = 0, n = mPoints.size(); i < n; i++) {
                ScalableGraphPoint point = mPoints.get(i);
                point.setScaleX(1.0f);
                point.setScaleY(1.0f / (Math.abs(point.getZ()) * lineLengthModifier * containerHeight) * strokeWidth);

                Animator animator = ValueAnimator.ofObject(new ScaleYEvaluator(SoundGraphView.this, point), 0.0f, 1.0f);

                if (mAnimationDurationMillis > 0) {
                    animator.setInterpolator(new OvershootInterpolator(OVERSHOOT_TENSION));
                    animator.setStartDelay(i * ANIMATION_START_DELAY);
                }

                animator.setDuration(mAnimationDurationMillis);

                animators.add(animator);
            }

            ((AnimatorSet) mAnimator).playTogether(animators);
            mAnimator.start();
        }
    }

    @Override
    public void stopAnimation() {
        synchronized (SoundGraphView.this) {
            if (mAnimator != null) {
                mAnimator.end();
            }
        }
    }

    @Override
    public void setPoints(List<? extends GraphPoint> points) {
        super.setPoints(points);
        setLineLengthModifierForRenderer();
    }

    @Override
    public void setLayoutManager(GraphLayoutManager layoutManager) {
        super.setLayoutManager(layoutManager);
        setLineLengthModifierForRenderer();
    }

    @Override
    public void setPointRenderer(GraphPointRenderer<ScalableGraphPoint> renderer) {
        if (!(renderer instanceof LineRenderer)) {
            throw new IllegalArgumentException("Incompatible point renderer: "
                    + SoundGraphView.class.getName() + " requires " + LineRenderer.class.getName());
        }
        super.setPointRenderer(renderer);
        setLineLengthModifierForRenderer();
    }

    @Override
    protected void setPointsInternal(List<? extends GraphPoint> points) {
        List<ScalableGraphPoint> internalPoints = new ArrayList<>();

        for (GraphPoint point : points) {
            internalPoints.add(new ScalableGraphPoint(point, 0.0f, 0.0f));
        }

        mPoints = internalPoints;
    }

    private void setLineLengthModifierForRenderer() {
        if (mPoints != null && mPointRenderer != null) {
            float maxAbsValue = Math.abs(mPoints.get(0).getZ());

            for (int i = 1, n = mPoints.size(); i < n; i++) {
                float absValue = Math.abs(mPoints.get(i).getZ());
                maxAbsValue = Math.max(maxAbsValue, absValue);
            }

            boolean noDifference = Float.compare(maxAbsValue, 0.0f) == 0;

            if (noDifference) {
                maxAbsValue = 1.0f;
            }

            float paddingTop = mLayoutManager != null ? mLayoutManager.getRelativePaddingTop() : 0.0f;
            float paddingBottom = mLayoutManager != null ? mLayoutManager.getRelativePaddingBottom() : 0.0f;

            float lineLengthModifier = (1.0f - (paddingTop + paddingBottom)) / maxAbsValue;

            ((LineRenderer) mPointRenderer).setLineLengthModifier(lineLengthModifier);
        }
    }

    public static class Builder extends GraphView.Builder<Builder, ScalableGraphPoint> {

        public Builder(Context context) {
            super(context);
        }

        @Override
        void createGraphView(Context context) {
            mGraphView = new SoundGraphView(context);
        }

    }

}
