package com.eightam.android.graphs.layout;

import com.eightam.android.graphs.point.GraphPoint;

import java.util.List;

public class SpiralArcLayoutManager implements GraphLayoutManager {

    private float mArcTotalLength;
    private float mGap;
    private float[][] mIntervals;

    public SpiralArcLayoutManager(float arcTotalLength, float gap) {
        mArcTotalLength = arcTotalLength;
        mGap = gap;
    }

    @Override
    public List<? extends GraphPoint> layout(List<? extends GraphPoint> points) {
        int numOfPoints = points.size();

        if (numOfPoints > 0) {
            float[][] intervals = new float[numOfPoints][];
            float sumAbsValue = 0;

            for (int i = 0; i < numOfPoints; i++) {
                sumAbsValue = sumAbsValue + Math.abs(points.get(i).getZ());
            }

            boolean noDifference = Float.compare(sumAbsValue, 0.0f) == 0;

            float maxSizeWithoutGaps = mArcTotalLength - mGap * (numOfPoints - 1);
            float spacing = 0;

            for (int i = 0; i < numOfPoints; i++) {
                float valueInterval = noDifference ?
                        maxSizeWithoutGaps / numOfPoints :
                        Math.abs(points.get(i).getZ()) / sumAbsValue * maxSizeWithoutGaps;

                if (spacing == 0) {
                    intervals[i] = new float[]{valueInterval, mArcTotalLength - valueInterval};
                } else {
                    intervals[i] = new float[]{0.0f, spacing, valueInterval, mArcTotalLength - (spacing + valueInterval)};
                }

                spacing = spacing + valueInterval + mGap;
            }

            mIntervals = intervals;
        }
        return points;
    }

    @Override
    public float getRelativePaddingLeft() {
        return 0;
    }

    @Override
    public float getRelativePaddingTop() {
        return 0;
    }

    @Override
    public float getRelativePaddingRight() {
        return 0;
    }

    @Override
    public float getRelativePaddingBottom() {
        return 0;
    }

    public float[][] getIntervals() {
        return mIntervals;
    }

}
