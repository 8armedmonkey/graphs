package com.eightam.android.graphs.behavior;

public interface Indexable {

    int getIndex();

}
