package com.eightam.android.graphs.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import com.eightam.android.graphs.layout.GraphLayoutManager;
import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.renderer.GraphPointRenderer;

import java.util.List;

public abstract class GraphView<T extends GraphPoint> extends View implements HasAnimation {

    protected List<T> mPoints;
    protected GraphLayoutManager mLayoutManager;
    protected GraphPointRenderer<T> mPointRenderer;
    protected long mAnimationDurationMillis;

    public GraphView(Context context) {
        super(context);
    }

    public GraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public List<? extends GraphPoint> getPoints() {
        return mPoints;
    }

    public void setPoints(List<? extends GraphPoint> points) {
        setPointsInternal(points);

        if (mLayoutManager != null) {
            mLayoutManager.layout(mPoints);
        }
    }

    public GraphLayoutManager getLayoutManager() {
        return mLayoutManager;
    }

    public void setLayoutManager(GraphLayoutManager layoutManager) {
        mLayoutManager = layoutManager;

        if (mPoints != null) {
            mLayoutManager.layout(mPoints);
        }
    }

    public GraphPointRenderer getPointRenderer() {
        return mPointRenderer;
    }

    public void setPointRenderer(GraphPointRenderer<T> renderer) {
        mPointRenderer = renderer;
    }

    public long getAnimationDurationMillis() {
        return mAnimationDurationMillis;
    }

    public void setAnimationDurationMillis(long animationDurationMillis) {
        mAnimationDurationMillis = animationDurationMillis;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();

        onPreRenderGraphPoints(canvas);

        if (mPoints != null && mPointRenderer != null) {
            renderPoints(canvas, mPoints, mPointRenderer);
        }

        onPostRenderGraphPoints(canvas);

        canvas.restore();
    }

    protected abstract void setPointsInternal(List<? extends GraphPoint> points);

    protected abstract void onPreRenderGraphPoints(Canvas canvas);

    protected abstract void renderPoints(Canvas canvas, List<T> points, GraphPointRenderer<T> graphPointRenderer);

    protected abstract void onPostRenderGraphPoints(Canvas canvas);

    /**
     * http://stackoverflow.com/questions/17164375/subclassing-a-java-builder-class
     */
    public abstract static class Builder<B extends Builder, T extends GraphPoint> {

        GraphView<T> mGraphView;

        public Builder(Context context) {
            createGraphView(context);
        }

        public B withPoints(List<? extends GraphPoint> points) {
            mGraphView.setPoints(points);
            return (B) Builder.this;
        }

        public B withLayoutManager(GraphLayoutManager layoutManager) {
            mGraphView.setLayoutManager(layoutManager);
            return (B) Builder.this;
        }

        public B withPointRenderer(GraphPointRenderer<T> renderer) {
            mGraphView.setPointRenderer(renderer);
            return (B) Builder.this;
        }

        public B withAnimationDurationMillis(long animationDurationMillis) {
            mGraphView.setAnimationDurationMillis(animationDurationMillis);
            return (B) Builder.this;
        }

        public GraphView build() {
            return mGraphView;
        }

        abstract void createGraphView(Context context);

    }

}
