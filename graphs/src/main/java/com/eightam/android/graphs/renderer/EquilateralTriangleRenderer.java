package com.eightam.android.graphs.renderer;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;

import com.eightam.android.graphs.point.ScalableGraphPoint;

public class EquilateralTriangleRenderer extends SimpleShapeRenderer<ScalableGraphPoint> {

    private static final float TRIANGLE_POINT_ANGLE_1 = 90.0f;
    private static final float TRIANGLE_POINT_ANGLE_2 = 210.0f;
    private static final float TRIANGLE_POINT_ANGLE_3 = 330.0f;
    private static final float TRIANGLE_ANGLE = 60.0f;
    private static final float FLIP_ROTATION_ANGLE = 180.0f;

    private float mMaxRelativeTriangleSide;
    private float mMinRelativeTriangleSide;
    private float mTriangleSideModifier;
    private Path mPath;

    private int mPositiveFillColor;
    private int mPositiveStrokeColor;
    private Shader mPositiveShader;

    private int mNegativeFillColor;
    private int mNegativeStrokeColor;
    private Shader mNegativeShader;

    private Matrix mShaderMatrix;

    public EquilateralTriangleRenderer(float maxRelativeTriangleSide, float minRelativeTriangleSide, float triangleSideModifier) {
        mMaxRelativeTriangleSide = maxRelativeTriangleSide;
        mMinRelativeTriangleSide = minRelativeTriangleSide;
        mTriangleSideModifier = triangleSideModifier;
        mPath = new Path();
        mShaderMatrix = new Matrix();
    }

    @Override
    public void render(Canvas canvas, int containerWidth, int containerHeight, ScalableGraphPoint graphPoint) {
        canvas.save();

        int shortestWidth = Math.min(containerWidth, containerHeight);

        float x = graphPoint.getX() * shortestWidth;
        float y = graphPoint.getY() * shortestWidth;
        float z = graphPoint.getZ();

        float maxSide = mMaxRelativeTriangleSide * shortestWidth;
        float minSide = mMinRelativeTriangleSide * shortestWidth;
        float side = Math.min(maxSide, Math.max(minSide, Math.abs(z) * mTriangleSideModifier * shortestWidth));
        float r = calculateTriangleRadiusBySide(side);

        float x1 = (float) (x + r * Math.cos(Math.toRadians(TRIANGLE_POINT_ANGLE_1)));
        float y1 = (float) (y + r * Math.sin(Math.toRadians(TRIANGLE_POINT_ANGLE_1)));
        float x2 = (float) (x + r * Math.cos(Math.toRadians(TRIANGLE_POINT_ANGLE_2)));
        float y2 = (float) (y + r * Math.sin(Math.toRadians(TRIANGLE_POINT_ANGLE_2)));
        float x3 = (float) (x + r * Math.cos(Math.toRadians(TRIANGLE_POINT_ANGLE_3)));
        float y3 = (float) (y + r * Math.sin(Math.toRadians(TRIANGLE_POINT_ANGLE_3)));

        float dy = (float) (r * Math.cos(Math.toRadians(TRIANGLE_ANGLE)));

        mPath.reset();
        mPath.moveTo(x1, y1);
        mPath.lineTo(x2, y2);
        mPath.lineTo(x3, y3);
        mPath.close();

        if (graphPoint.getZ() > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(FLIP_ROTATION_ANGLE, x, y);
            matrix.postTranslate(0, -dy);

            mPath.transform(matrix);
        } else {
            Matrix matrix = new Matrix();
            matrix.postTranslate(0, dy);

            mPath.transform(matrix);
        }

        Shader shader = Float.compare(z, 0.0f) > 0 ? mPositiveShader : mNegativeShader;

        if (shader != null) {
            shader.getLocalMatrix(mShaderMatrix);

            mShaderMatrix.reset();
            mShaderMatrix.postScale(side, 1.0f);
            mShaderMatrix.postTranslate(x1, 0.0f);

            shader.setLocalMatrix(mShaderMatrix);
        }

        if (mShouldFill) {
            int fillColor = Float.compare(z, 0.0f) > 0 ? mPositiveFillColor : mNegativeFillColor;

            mPaint.setStyle(Paint.Style.FILL);

            if (shader != null) {
                mPaint.setShader(shader);
            } else {
                mPaint.setColor(fillColor);
            }

            canvas.save();
            canvas.scale(graphPoint.getScaleX(), graphPoint.getScaleY(), x, y);
            canvas.drawPath(mPath, mPaint);
            canvas.restore();
        }

        if (mShouldStroke) {
            int strokeColor = Float.compare(z, 0.0f) > 0 ? mPositiveStrokeColor : mNegativeStrokeColor;

            mPaint.setStyle(Paint.Style.STROKE);

            if (shader != null) {
                mPaint.setShader(shader);
            } else {
                mPaint.setColor(strokeColor);
            }

            canvas.save();
            canvas.scale(graphPoint.getScaleX(), graphPoint.getScaleY(), x, y);
            canvas.drawPath(mPath, mPaint);
            canvas.restore();
        }

        canvas.restore();
    }

    public float getTriangleSideModifier() {
        return mTriangleSideModifier;
    }

    public void setTriangleSideModifier(float triangleSideModifier) {
        mTriangleSideModifier = triangleSideModifier;
    }

    public int getPositiveFillColor() {
        return mPositiveFillColor;
    }

    public void setPositiveFillColor(int positiveFillColor) {
        mPositiveFillColor = positiveFillColor;
    }

    public int getPositiveStrokeColor() {
        return mPositiveStrokeColor;
    }

    public void setPositiveStrokeColor(int positiveStrokeColor) {
        mPositiveStrokeColor = positiveStrokeColor;
    }

    public Shader getPositiveShader() {
        return mPositiveShader;
    }

    public void setPositiveShader(Shader positiveShader) {
        mPositiveShader = positiveShader;
    }

    public int getNegativeFillColor() {
        return mNegativeFillColor;
    }

    public void setNegativeFillColor(int negativeFillColor) {
        mNegativeFillColor = negativeFillColor;
    }

    public int getNegativeStrokeColor() {
        return mNegativeStrokeColor;
    }

    public void setNegativeStrokeColor(int negativeStrokeColor) {
        mNegativeStrokeColor = negativeStrokeColor;
    }

    public Shader getNegativeShader() {
        return mNegativeShader;
    }

    public void setNegativeShader(Shader negativeShader) {
        mNegativeShader = negativeShader;
    }

    private float calculateTriangleRadiusBySide(float side) {
        return (float) Math.abs(side / (2 * Math.sin(Math.toDegrees(TRIANGLE_ANGLE))));
    }

}
