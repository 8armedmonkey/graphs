package com.eightam.android.graphs.renderer;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Shader;

import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.point.IndexableGraphPoint;

import java.util.List;

public class BezierCurveRenderer implements GraphLineRenderer<IndexableGraphPoint> {

    private List<? extends GraphPoint> mPoints;
    private Path mPath;
    private RectF mPathBounds;
    private Paint mPaint;
    private Matrix mShaderMatrix;

    public BezierCurveRenderer() {
        this(null);
    }

    /**
     * The points should have been processed by the layout manager first.
     * @param points
     */
    public BezierCurveRenderer(List<? extends GraphPoint> points) {
        mPoints = points;

        mPath = new Path();
        mPathBounds = new RectF();

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStyle(Paint.Style.STROKE);

        mShaderMatrix = new Matrix();
    }

    @Override
    public void render(Canvas canvas, int containerWidth, int containerHeight, IndexableGraphPoint p1, IndexableGraphPoint p2) {
        canvas.save();

        if (mPoints != null) {
            int shortestWidth = Math.min(containerWidth, containerHeight);
            int numOfPoints = mPoints.size();

            float dx1, dy1, dx2, dy2;

            if (p1.getIndex() == 0) {
                dx1 = 0.33f * (p2.getX() - p1.getX());
                dy1 = 0.33f * (p2.getY() - p1.getY());
            } else {
                GraphPoint p0 = mPoints.get(p1.getIndex() - 1);

                dx1 = 0.33f * (p2.getX() - p0.getX());
                dy1 = 0.33f * (p2.getY() - p0.getY());
            }

            if (p2.getIndex() == numOfPoints - 1) {
                dx2 = 0.33f * (p2.getX() - p1.getX());
                dy2 = 0.33f * (p2.getY() - p1.getY());
            } else {
                GraphPoint p3 = mPoints.get(p2.getIndex() + 1);

                dx2 = 0.33f * (p3.getX() - p1.getX());
                dy2 = 0.33f * (p3.getY() - p1.getY());
            }

            float cx1 = (p1.getX() + dx1) * shortestWidth;
            float cy1 = (p1.getY() + dy1) * shortestWidth;
            float cx2 = (p2.getX() - dx2) * shortestWidth;
            float cy2 = (p2.getY() - dy2) * shortestWidth;
            float x1 = p1.getX() * shortestWidth;
            float y1 = p1.getY() * shortestWidth;
            float x2 = p2.getX() * shortestWidth;
            float y2 = p2.getY() * shortestWidth;

            if (p1.getIndex() == 0) {
                mPath.reset();
                mPath.moveTo(x1, y1);
            }

            mPath.cubicTo(cx1, cy1, cx2, cy2, x2, y2);

            if (p2.getIndex() == numOfPoints - 1) {
                Shader shader = mPaint.getShader();

                if (shader != null) {
                    shader.getLocalMatrix(mShaderMatrix);
                    mPath.computeBounds(mPathBounds, true);

                    mShaderMatrix.reset();
                    mShaderMatrix.postScale(mPathBounds.width(), 1.0f);
                    mShaderMatrix.postTranslate(mPathBounds.left, 0.0f);

                    shader.setLocalMatrix(mShaderMatrix);
                }

                canvas.drawPath(mPath, mPaint);
            }
        }

        canvas.restore();
    }

    /**
     * The points should have been processed by the layout manager first.
     * @param points
     */
    public void setPoints(List<? extends GraphPoint> points) {
        mPoints = points;
    }

    public void setStrokeWidth(float strokeWidth) {
        mPaint.setStrokeWidth(strokeWidth);
    }

    public void setStrokeColor(int color) {
        mPaint.setColor(color);
    }

    public void setShader(Shader shader) {
        mPaint.setShader(shader);
    }

}
