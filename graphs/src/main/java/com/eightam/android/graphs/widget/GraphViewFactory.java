package com.eightam.android.graphs.widget;

import android.content.Context;
import android.graphics.LinearGradient;
import android.graphics.Shader;

import com.eightam.android.graphs.R;
import com.eightam.android.graphs.layout.BubbleLayoutManager;
import com.eightam.android.graphs.layout.CenterVerticalLayoutManager;
import com.eightam.android.graphs.layout.EquilateralTriangleLayoutManager;
import com.eightam.android.graphs.layout.GraphLayoutManager;
import com.eightam.android.graphs.layout.SpiralArcLayoutManager;
import com.eightam.android.graphs.layout.StandardLayoutManager;
import com.eightam.android.graphs.model.GraphType;
import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.point.IndexableGraphPoint;
import com.eightam.android.graphs.point.ScalableGraphPoint;
import com.eightam.android.graphs.point.TranslatableGraphPoint;
import com.eightam.android.graphs.renderer.BezierCurveRenderer;
import com.eightam.android.graphs.renderer.BubbleRenderer;
import com.eightam.android.graphs.renderer.EquilateralTriangleRenderer;
import com.eightam.android.graphs.renderer.FixedRadiusCircleRenderer;
import com.eightam.android.graphs.renderer.GraphLineRenderer;
import com.eightam.android.graphs.renderer.GraphPointRenderer;
import com.eightam.android.graphs.renderer.LineRenderer;
import com.eightam.android.graphs.renderer.NothingRenderer;
import com.eightam.android.graphs.renderer.SimpleGraphLineRenderer;
import com.eightam.android.graphs.renderer.SpiralArcRenderer;

import java.util.List;

public class GraphViewFactory {

    public static GraphView createGraphView(Context context, GraphType graphType, List<GraphPoint> points) {
        switch (graphType) {
            case BUBBLES: {
                return createBubbleGraphView(context, points);
            }

            case MODERN: {
                return createModernGraphView(context, points);
            }

            case SOUND: {
                return createSoundGraphView(context, points);
            }

            case CLASSIC: {
                return createClassicGraphView(context, points);
            }

            case WAVE: {
                return createWaveGraphView(context, points);
            }

            case INFINITE: {
                return createInfiniteGraphView(context, points);
            }

            default: {
                throw new IllegalArgumentException("Invalid graph type.");
            }
        }
    }

    private static GraphView createBubbleGraphView(Context context, List<GraphPoint> points) {
        float maxBubbleRadius = context.getResources().getFraction(R.fraction.graph__bubble_max_radius, 1, 1);
        float minBubbleRadius = context.getResources().getFraction(R.fraction.graph__bubble_min_radius, 1, 1);
        float gap = context.getResources().getFraction(R.fraction.graph__bubble_gap, 1, 1);
        float hPadding = context.getResources().getFraction(R.fraction.graph__bubble_horizontal_padding, 1, 1);
        float vPadding = context.getResources().getFraction(R.fraction.graph__bubble_horizontal_padding, 1, 1);

        int positiveValueGradientStartColor = context.getResources().getColor(R.color.graph__positive_value_gradient_start_color);
        int positiveValueGradientEndColor = context.getResources().getColor(R.color.graph__positive_value_gradient_end_color);
        int negativeValueGradientStartColor = context.getResources().getColor(R.color.graph__negative_value_gradient_start_color);
        int negativeValueGradientEndColor = context.getResources().getColor(R.color.graph__negative_value_gradient_end_color);

        Shader positiveShader = new LinearGradient(0.0f, 0.0f, 1.0f, 0.0f,
                new int[]{positiveValueGradientStartColor, positiveValueGradientEndColor}, null, Shader.TileMode.CLAMP);
        Shader negativeShader = new LinearGradient(0.0f, 0.0f, 1.0f, 0.0f,
                new int[]{negativeValueGradientStartColor, negativeValueGradientEndColor}, null, Shader.TileMode.CLAMP);

        long animationDurationMillis = context.getResources().getInteger(R.integer.graph__generic_animation_duration_millis);

        GraphLayoutManager layoutManager = new BubbleLayoutManager(
                maxBubbleRadius, minBubbleRadius, gap,
                hPadding, vPadding, hPadding, vPadding);

        GraphPointRenderer<ScalableGraphPoint> pointRenderer = new BubbleRenderer(
                maxBubbleRadius, minBubbleRadius, 0.0f,
                hPadding, vPadding, hPadding, vPadding);
        ((BubbleRenderer) pointRenderer).setShouldFill(true);
        ((BubbleRenderer) pointRenderer).setShouldStroke(false);
        ((BubbleRenderer) pointRenderer).setPositiveShader(positiveShader);
        ((BubbleRenderer) pointRenderer).setNegativeShader(negativeShader);

        return new BubbleGraphView.Builder(context)
                .withPoints(points)
                .withLayoutManager(layoutManager)
                .withPointRenderer(pointRenderer)
                .withAnimationDurationMillis(animationDurationMillis)
                .build();
    }

    private static GraphView createModernGraphView(Context context, List<GraphPoint> points) {
        float maxTriangleSide = context.getResources().getFraction(R.fraction.graph__modern_triangle_max_side, 1, 1);
        float minTriangleSide = context.getResources().getFraction(R.fraction.graph__modern_triangle_min_side, 1, 1);
        float gap = context.getResources().getFraction(R.fraction.graph__modern_gap, 1, 1);

        int positiveValueGradientStartColor = context.getResources().getColor(R.color.graph__positive_value_gradient_start_color);
        int positiveValueGradientEndColor = context.getResources().getColor(R.color.graph__positive_value_gradient_end_color);
        int negativeValueGradientStartColor = context.getResources().getColor(R.color.graph__negative_value_gradient_start_color);
        int negativeValueGradientEndColor = context.getResources().getColor(R.color.graph__negative_value_gradient_end_color);

        Shader positiveShader = new LinearGradient(0.0f, 0.0f, 1.0f, 0.0f,
                new int[]{positiveValueGradientStartColor, positiveValueGradientEndColor}, null, Shader.TileMode.CLAMP);
        Shader negativeShader = new LinearGradient(0.0f, 0.0f, 1.0f, 0.0f,
                new int[]{negativeValueGradientStartColor, negativeValueGradientEndColor}, null, Shader.TileMode.CLAMP);

        long animationDurationMillis = context.getResources().getInteger(R.integer.graph__generic_animation_duration_millis);

        GraphLayoutManager layoutManager = new EquilateralTriangleLayoutManager(maxTriangleSide, minTriangleSide, gap);

        GraphPointRenderer<ScalableGraphPoint> pointRenderer = new EquilateralTriangleRenderer(maxTriangleSide, minTriangleSide, 0.0f);
        ((EquilateralTriangleRenderer) pointRenderer).setShouldFill(true);
        ((EquilateralTriangleRenderer) pointRenderer).setShouldStroke(false);
        ((EquilateralTriangleRenderer) pointRenderer).setPositiveShader(positiveShader);
        ((EquilateralTriangleRenderer) pointRenderer).setNegativeShader(negativeShader);

        return new ModernGraphView.Builder(context)
                .withPoints(points)
                .withLayoutManager(layoutManager)
                .withPointRenderer(pointRenderer)
                .withAnimationDurationMillis(animationDurationMillis)
                .build();
    }

    private static GraphView createSoundGraphView(Context context, List<GraphPoint> points) {
        float hPadding = context.getResources().getFraction(R.fraction.graph__sound_horizontal_padding, 1, 1);
        float vPadding = context.getResources().getFraction(R.fraction.graph__sound_vertical_padding, 1, 1);

        float strokeWidth = context.getResources().getDimensionPixelSize(R.dimen.graph__sound_graph_stroke_width);

        int positiveValueGradientStartColor = context.getResources().getColor(R.color.graph__positive_value_gradient_start_color);
        int positiveValueGradientEndColor = context.getResources().getColor(R.color.graph__positive_value_gradient_end_color);
        int negativeValueGradientStartColor = context.getResources().getColor(R.color.graph__negative_value_gradient_start_color);
        int negativeValueGradientEndColor = context.getResources().getColor(R.color.graph__negative_value_gradient_end_color);

        Shader positiveShader = new LinearGradient(0.0f, 0.0f, 1.0f, 0.0f,
                new int[]{positiveValueGradientStartColor, positiveValueGradientEndColor}, null, Shader.TileMode.CLAMP);
        Shader negativeShader = new LinearGradient(0.0f, 0.0f, 1.0f, 0.0f,
                new int[]{negativeValueGradientStartColor, negativeValueGradientEndColor}, null, Shader.TileMode.CLAMP);

        long animationDurationMillis = context.getResources().getInteger(R.integer.graph__generic_animation_duration_millis);

        GraphLayoutManager layoutManager = new CenterVerticalLayoutManager(hPadding, vPadding, hPadding, vPadding);

        GraphPointRenderer<ScalableGraphPoint> pointRenderer = new LineRenderer(0.0f);
        ((LineRenderer) pointRenderer).setShouldFill(false);
        ((LineRenderer) pointRenderer).setShouldStroke(true);
        ((LineRenderer) pointRenderer).setStrokeWidth(strokeWidth);
        ((LineRenderer) pointRenderer).setPositiveShader(positiveShader);
        ((LineRenderer) pointRenderer).setNegativeShader(negativeShader);

        return new SoundGraphView.Builder(context)
                .withPoints(points)
                .withLayoutManager(layoutManager)
                .withPointRenderer(pointRenderer)
                .withAnimationDurationMillis(animationDurationMillis)
                .build();
    }

    private static GraphView createClassicGraphView(Context context, List<GraphPoint> points) {
        float hPadding = context.getResources().getFraction(R.fraction.graph__classic_horizontal_padding, 1, 1);
        float vPadding = context.getResources().getFraction(R.fraction.graph__classic_vertical_padding, 1, 1);

        float circleRadius = context.getResources().getDimensionPixelSize(R.dimen.graph__classic_line_graph_point_circle_radius);
        float pointStrokeWidth = context.getResources().getDimensionPixelSize(R.dimen.graph__classic_line_graph_point_stroke_width);
        float lineStrokeWidth = context.getResources().getDimensionPixelSize(R.dimen.graph__classic_line_graph_line_stroke_width);

        int lineStrokeColor = context.getResources().getColor(R.color.graph__positive_value_color);
        int pointStrokeColor = context.getResources().getColor(R.color.graph__negative_value_color);

        long animationDurationMillis = context.getResources().getInteger(R.integer.graph__generic_animation_duration_millis);

        GraphLayoutManager layoutManager = new StandardLayoutManager(hPadding, vPadding, hPadding, vPadding);

        GraphPointRenderer<TranslatableGraphPoint> pointRenderer = new FixedRadiusCircleRenderer(circleRadius);
        ((FixedRadiusCircleRenderer) pointRenderer).setShouldFill(false);
        ((FixedRadiusCircleRenderer) pointRenderer).setShouldStroke(true);
        ((FixedRadiusCircleRenderer) pointRenderer).setStrokeColor(pointStrokeColor);
        ((FixedRadiusCircleRenderer) pointRenderer).setStrokeWidth(pointStrokeWidth);

        GraphLineRenderer<TranslatableGraphPoint> lineRenderer = new SimpleGraphLineRenderer();
        ((SimpleGraphLineRenderer) lineRenderer).setStrokeWidth(lineStrokeWidth);
        ((SimpleGraphLineRenderer) lineRenderer).setStrokeColor(lineStrokeColor);

        return new ClassicLineGraphView.Builder(context)
                .withPoints(points)
                .withLayoutManager(layoutManager)
                .withPointRenderer(pointRenderer)
                .withLineRenderer(lineRenderer)
                .withAnimationDurationMillis(animationDurationMillis)
                .build();
    }

    private static GraphView createWaveGraphView(Context context, List<GraphPoint> points) {
        float hPadding = context.getResources().getFraction(R.fraction.graph__wave_horizontal_padding, 1, 1);
        float vPadding = context.getResources().getFraction(R.fraction.graph__wave_vertical_padding, 1, 1);

        float strokeWidth = context.getResources().getDimensionPixelSize(R.dimen.graph__wave_graph_stroke_width);

        int positiveValueColor = context.getResources().getColor(R.color.graph__positive_value_color);
        int negativeValueColor = context.getResources().getColor(R.color.graph__negative_value_color);

        Shader shader = new LinearGradient(0.0f, 0.0f, 1.0f, 0.0f,
                new int[]{positiveValueColor, negativeValueColor, positiveValueColor}, null, Shader.TileMode.CLAMP);

        long animationDurationMillis = context.getResources().getInteger(R.integer.graph__generic_animation_duration_millis);

        GraphLayoutManager layoutManager = new StandardLayoutManager(hPadding, vPadding, hPadding, vPadding);

        GraphPointRenderer<IndexableGraphPoint> pointRenderer = new NothingRenderer<>();

        GraphLineRenderer<IndexableGraphPoint> lineRenderer = new BezierCurveRenderer();
        ((BezierCurveRenderer) lineRenderer).setStrokeWidth(strokeWidth);
        ((BezierCurveRenderer) lineRenderer).setShader(shader);

        return new WaveGraphView.Builder(context)
                .withPoints(points)
                .withLayoutManager(layoutManager)
                .withPointRenderer(pointRenderer)
                .withLineRenderer(lineRenderer)
                .withAnimationDurationMillis(animationDurationMillis)
                .build();
    }

    private static GraphView createInfiniteGraphView(Context context, List<GraphPoint> points) {
        float strokeWidth = context.getResources().getDimensionPixelSize(R.dimen.graph__infinite_graph_stroke_width);
        float gap = context.getResources().getFraction(R.fraction.graph__infinite_gap, 1, 1);

        int positiveArcColor = context.getResources().getColor(R.color.graph__positive_value_color);
        int negativeArcColor = context.getResources().getColor(R.color.graph__negative_value_color);

        long animationDurationMillis = context.getResources().getInteger(R.integer.graph__generic_animation_duration_millis);

        GraphLayoutManager layoutManager = new SpiralArcLayoutManager(SpiralArcRenderer.ARC_TOTAL_LENGTH, gap);
        layoutManager.layout(points);

        float[][] intervals = ((SpiralArcLayoutManager) layoutManager).getIntervals();

        GraphPointRenderer<IndexableGraphPoint> pointRenderer = new SpiralArcRenderer(intervals, positiveArcColor, negativeArcColor);
        ((SpiralArcRenderer) pointRenderer).setShouldStroke(true);
        ((SpiralArcRenderer) pointRenderer).setStrokeWidth(strokeWidth);

        return new InfiniteGraphView.Builder(context)
                .withPoints(points)
                .withLayoutManager(layoutManager)
                .withPointRenderer(pointRenderer)
                .withAnimationDurationMillis(animationDurationMillis)
                .build();
    }

}
