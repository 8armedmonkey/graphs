package com.eightam.android.graphs.point;

import com.eightam.android.graphs.behavior.Scalable;

public class ScalableGraphPoint extends GraphPoint implements Scalable {

    protected float mScaleX;
    protected float mScaleY;

    public ScalableGraphPoint() {
        this(0.0f, 0.0f, 0.0f);
    }

    public ScalableGraphPoint(float x, float y, float z) {
        this(x, y, z, 1.0f, 1.0f);
    }

    public ScalableGraphPoint(float x, float y, float z, float scaleX, float scaleY) {
        super(x, y, z);
        mScaleX = scaleX;
        mScaleY = scaleY;
    }

    public ScalableGraphPoint(GraphPoint point) {
        this(point.mX, point.mY, point.mZ);
    }

    public ScalableGraphPoint(GraphPoint point, float scaleX, float scaleY) {
        this(point.mX, point.mY, point.mZ, scaleX, scaleY);
    }

    @Override
    public float getScaleX() {
        return mScaleX;
    }

    @Override
    public void setScaleX(float scaleX) {
        mScaleX = scaleX;
    }

    @Override
    public float getScaleY() {
        return mScaleY;
    }

    @Override
    public void setScaleY(float scaleY) {
        mScaleY = scaleY;
    }

}
