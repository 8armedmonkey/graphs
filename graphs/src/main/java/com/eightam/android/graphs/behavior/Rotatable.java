package com.eightam.android.graphs.behavior;

public interface Rotatable {

    float getRotationAngle();

    void setRotationAngle(float rotationAngle);

}
