package com.eightam.android.graphs.layout;

import com.eightam.android.graphs.point.GraphPoint;

import java.util.List;

public interface GraphLayoutManager {

    List<? extends GraphPoint> layout(List<? extends GraphPoint> points);

    float getRelativePaddingLeft();

    float getRelativePaddingTop();

    float getRelativePaddingRight();

    float getRelativePaddingBottom();

}
