package com.eightam.android.graphs.model;

public enum GraphType {

    BUBBLES("bubbles"),
    MODERN("modern"),
    SOUND("sound"),
    CLASSIC("classic"),
    WAVE("wave"),
    INFINITE("infinite");

    String mStringValue;

    GraphType(String stringValue) {
        mStringValue = stringValue;
    }

    public String toString() {
        return mStringValue;
    }

}
