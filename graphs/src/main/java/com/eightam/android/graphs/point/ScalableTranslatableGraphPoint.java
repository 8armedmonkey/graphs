package com.eightam.android.graphs.point;

import com.eightam.android.graphs.behavior.Translatable;

public class ScalableTranslatableGraphPoint extends ScalableGraphPoint implements Translatable {

    protected float mTranslationX;
    protected float mTranslationY;

    public ScalableTranslatableGraphPoint() {
        this(0.0f, 0.0f, 0.0f);
    }

    public ScalableTranslatableGraphPoint(float x, float y, float z) {
        this(x, y, z, 1.0f, 1.0f, x, y);
    }

    public ScalableTranslatableGraphPoint(float x, float y, float z,
                                          float scaleX, float scaleY,
                                          float translationX, float translationY) {
        super(x, y, z, scaleX, scaleY);
        mTranslationX = translationX;
        mTranslationY = translationY;
    }

    public ScalableTranslatableGraphPoint(GraphPoint graphPoint) {
        this(graphPoint.mX, graphPoint.mY, graphPoint.mZ);
    }

    public ScalableTranslatableGraphPoint(GraphPoint graphPoint,
                                          float scaleX, float scaleY,
                                          float translationX, float translationY) {
        this(graphPoint.mX, graphPoint.mY, graphPoint.mZ, scaleX, scaleY, translationX, translationY);
    }

    @Override
    public float getTranslationX() {
        return mTranslationX;
    }

    @Override
    public void setTranslationX(float translationX) {
        mTranslationX = translationX;
    }

    @Override
    public float getTranslationY() {
        return mTranslationY;
    }

    @Override
    public void setTranslationY(float translationY) {
        mTranslationY = translationY;
    }

}
