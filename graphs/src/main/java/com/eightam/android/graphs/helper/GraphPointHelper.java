package com.eightam.android.graphs.helper;

import com.eightam.android.graphs.point.GraphPoint;

import java.util.ArrayList;
import java.util.List;

public class GraphPointHelper {

    public static List<GraphPoint> toPoints(float[] values) {
        List<GraphPoint> points = new ArrayList<>();

        for (float value : values) {
            points.add(new GraphPoint(value));
        }
        return points;
    }

}
