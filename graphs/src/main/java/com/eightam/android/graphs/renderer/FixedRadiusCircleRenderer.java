package com.eightam.android.graphs.renderer;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import com.eightam.android.graphs.point.TranslatableGraphPoint;

public class FixedRadiusCircleRenderer extends SimpleShapeRenderer<TranslatableGraphPoint> {

    private float mCircleRadius;
    private RectF mOvalRect;

    public FixedRadiusCircleRenderer(float circleRadius) {
        super();
        mCircleRadius = circleRadius;
        mOvalRect = new RectF();
    }

    @Override
    public void render(Canvas canvas, int containerWidth, int containerHeight, TranslatableGraphPoint graphPoint) {
        canvas.save();

        int shortestWidth = Math.min(containerWidth, containerHeight);
        float x = (graphPoint.getX() + graphPoint.getTranslationX()) * shortestWidth;
        float y = (graphPoint.getY() + graphPoint.getTranslationY()) * shortestWidth;

        mOvalRect.set(x - mCircleRadius, y - mCircleRadius, x + mCircleRadius, y + mCircleRadius);

        if (mShouldFill) {
            mPaint.setStyle(Paint.Style.FILL);

            if (mShader != null) {
                mPaint.setShader(mShader);
            } else {
                mPaint.setColor(mFillColor);
            }

            canvas.drawOval(mOvalRect, mPaint);
        }

        if (mShouldStroke) {
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setColor(mStrokeColor);
            canvas.drawOval(mOvalRect, mPaint);
        }

        canvas.restore();
    }

    public float getCircleRadius() {
        return mCircleRadius;
    }

    public void setCircleRadius(float circleRadius) {
        mCircleRadius = circleRadius;
    }

}
