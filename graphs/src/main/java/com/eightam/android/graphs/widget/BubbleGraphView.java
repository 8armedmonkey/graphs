package com.eightam.android.graphs.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.OvershootInterpolator;

import com.eightam.android.graphs.animation.ScaleXYEvaluator;
import com.eightam.android.graphs.layout.BubbleLayoutManager;
import com.eightam.android.graphs.layout.GraphLayoutManager;
import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.point.ScalableGraphPoint;
import com.eightam.android.graphs.renderer.BubbleRenderer;
import com.eightam.android.graphs.renderer.GraphPointRenderer;

import java.util.ArrayList;
import java.util.List;

public class BubbleGraphView extends SimpleGraphView<ScalableGraphPoint> {

    private static final long ANIMATION_START_DELAY = 200;
    private static final float OVERSHOOT_TENSION = 2.0f;

    private Animator mAnimator;

    public BubbleGraphView(Context context) {
        super(context);
    }

    public BubbleGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BubbleGraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void startAnimation() {
        synchronized (BubbleGraphView.this) {
            stopAnimation();

            if (mPoints != null) {
                mAnimator = new AnimatorSet();
                List<Animator> animators = new ArrayList<>();

                for (int i = 0, n = mPoints.size(); i < n; i++) {
                    ScalableGraphPoint point = mPoints.get(i);
                    point.setScaleX(0.0f);
                    point.setScaleY(0.0f);

                    Animator animator = ValueAnimator.ofObject(new ScaleXYEvaluator(BubbleGraphView.this, point), 0.0f, 1.0f);

                    if (mAnimationDurationMillis > 0) {
                        animator.setInterpolator(new OvershootInterpolator(OVERSHOOT_TENSION));
                        animator.setStartDelay(i * ANIMATION_START_DELAY);
                    }

                    animator.setDuration(mAnimationDurationMillis);

                    animators.add(animator);
                }

                ((AnimatorSet) mAnimator).playTogether(animators);
                mAnimator.start();
            }
        }
    }

    @Override
    public void stopAnimation() {
        synchronized (BubbleGraphView.this) {
            if (mAnimator != null) {
                mAnimator.end();
            }
        }
    }

    @Override
    public void setPoints(List<? extends GraphPoint> points) {
        super.setPoints(points);
        setBubbleRadiusModifierForRenderer();
    }

    @Override
    public void setLayoutManager(GraphLayoutManager layoutManager) {
        if (!(layoutManager instanceof BubbleLayoutManager)) {
            throw new IllegalArgumentException("Incompatible layout manager: "
                    + BubbleGraphView.class.getName() + " requires " + BubbleLayoutManager.class.getName());
        }
        super.setLayoutManager(layoutManager);
    }

    @Override
    public void setPointRenderer(GraphPointRenderer<ScalableGraphPoint> renderer) {
        if (!(renderer instanceof BubbleRenderer)) {
            throw new IllegalArgumentException("Incompatible point renderer: "
                    + BubbleGraphView.class.getName() + " requires " + BubbleRenderer.class.getName());
        }
        super.setPointRenderer(renderer);
        setBubbleRadiusModifierForRenderer();
    }

    @Override
    protected void setPointsInternal(List<? extends GraphPoint> points) {
        List<ScalableGraphPoint> internalPoints = new ArrayList<>();

        for (GraphPoint point : points) {
            internalPoints.add(new ScalableGraphPoint(point, 0.0f, 0.0f));
        }

        mPoints = internalPoints;
    }

    private void setBubbleRadiusModifierForRenderer() {
        if (mPoints != null && mPointRenderer != null) {
            float sumAbsValue = 0.0f;

            for (GraphPoint point : mPoints) {
                sumAbsValue = sumAbsValue + Math.abs(point.getZ());
            }

            boolean noDifference = Float.compare(sumAbsValue, 0.0f) == 0;

            if (noDifference) {
                sumAbsValue = 1.0f;
            }

            float bubbleRadiusModifier = 1.0f / sumAbsValue;

            ((BubbleRenderer) mPointRenderer).setBubbleRadiusModifier(bubbleRadiusModifier);
        }
    }

    public static class Builder extends GraphView.Builder<Builder, ScalableGraphPoint> {

        public Builder(Context context) {
            super(context);
        }

        @Override
        void createGraphView(Context context) {
            mGraphView = new BubbleGraphView(context);
        }

    }

}
