package com.eightam.android.graphs.animation;

import com.eightam.android.graphs.behavior.Translatable;
import com.eightam.android.graphs.widget.GraphView;

public class TranslateYEvaluator extends GraphAnimationEvaluator {

    private Translatable mTranslatable;

    public TranslateYEvaluator(GraphView graphView, Translatable translatable) {
        super(graphView);
        mTranslatable = translatable;
    }

    @Override
    protected void useValue(float value) {
        mTranslatable.setTranslationY(value);
    }

}
