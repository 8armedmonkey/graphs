package com.eightam.android.graphs.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.eightam.android.graphs.point.GraphPoint;

public abstract class AutoCenteringGraphView<T extends GraphPoint> extends GraphView<T> {

    public AutoCenteringGraphView(Context context) {
        super(context);
    }

    public AutoCenteringGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AutoCenteringGraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onPreRenderGraphPoints(Canvas canvas) {
        int viewWidth = getWidth();
        int viewHeight = getHeight();
        int shortestWidth = Math.min(viewWidth, viewHeight);

        canvas.translate(0.5f * (viewWidth - shortestWidth), 0.5f * (viewHeight - shortestWidth));
    }

}
