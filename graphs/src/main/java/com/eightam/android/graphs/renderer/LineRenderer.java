package com.eightam.android.graphs.renderer;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;

import com.eightam.android.graphs.point.ScalableGraphPoint;

public class LineRenderer extends SimpleShapeRenderer<ScalableGraphPoint> {

    private float mLineLengthModifier;

    private int mPositiveFillColor;
    private int mPositiveStrokeColor;
    private Shader mPositiveShader;

    private int mNegativeFillColor;
    private int mNegativeStrokeColor;
    private Shader mNegativeShader;

    private Matrix mShaderMatrix;

    public LineRenderer(float lineLengthModifier) {
        mLineLengthModifier = lineLengthModifier;
        mShaderMatrix = new Matrix();
    }

    @Override
    public void render(Canvas canvas, int containerWidth, int containerHeight, ScalableGraphPoint graphPoint) {
        canvas.save();

        int shortestWidth = Math.min(containerWidth, containerHeight);

        float x = graphPoint.getX() * shortestWidth;
        float y = graphPoint.getY() * shortestWidth;
        float z = graphPoint.getZ();

        float r = Math.max(0.5f * mPaint.getStrokeWidth(), 0.5f * Math.abs(z) * mLineLengthModifier * containerHeight);

        Shader shader = Float.compare(z, 0.0f) > 0 ? mPositiveShader : mNegativeShader;

        if (shader != null) {
            shader.getLocalMatrix(mShaderMatrix);

            mShaderMatrix.reset();
            mShaderMatrix.postScale(mPaint.getStrokeWidth(), 1.0f);
            mShaderMatrix.postTranslate(x - 0.5f * mPaint.getStrokeWidth(), 0.0f);

            shader.setLocalMatrix(mShaderMatrix);
        }

        if (mShouldFill) {
            int fillColor = Float.compare(z, 0.0f) > 0 ? mPositiveFillColor : mNegativeFillColor;

            mPaint.setStyle(Paint.Style.FILL);

            if (shader != null) {
                mPaint.setShader(shader);
            } else {
                mPaint.setColor(fillColor);
            }

            canvas.save();
            canvas.scale(graphPoint.getScaleX(), graphPoint.getScaleY(), x, y);
            canvas.drawLine(x, y - r, x, y + r, mPaint);
            canvas.restore();
        }

        if (mShouldStroke) {
            int strokeColor = Float.compare(z, 0.0f) > 0 ? mPositiveStrokeColor : mNegativeStrokeColor;

            mPaint.setStyle(Paint.Style.STROKE);

            if (shader != null) {
                mPaint.setShader(shader);
            } else {
                mPaint.setColor(strokeColor);
            }

            canvas.save();
            canvas.scale(graphPoint.getScaleX(), graphPoint.getScaleY(), x, y);
            canvas.drawLine(x, y - r, x, y + r, mPaint);
            canvas.restore();
        }

        canvas.restore();
    }

    public float getLineLengthModifier() {
        return mLineLengthModifier;
    }

    public void setLineLengthModifier(float lineLengthModifier) {
        mLineLengthModifier = lineLengthModifier;
    }

    public int getPositiveFillColor() {
        return mPositiveFillColor;
    }

    public void setPositiveFillColor(int positiveFillColor) {
        mPositiveFillColor = positiveFillColor;
    }

    public int getPositiveStrokeColor() {
        return mPositiveStrokeColor;
    }

    public void setPositiveStrokeColor(int positiveStrokeColor) {
        mPositiveStrokeColor = positiveStrokeColor;
    }

    public Shader getPositiveShader() {
        return mPositiveShader;
    }

    public void setPositiveShader(Shader positiveShader) {
        mPositiveShader = positiveShader;
    }

    public int getNegativeFillColor() {
        return mNegativeFillColor;
    }

    public void setNegativeFillColor(int negativeFillColor) {
        mNegativeFillColor = negativeFillColor;
    }

    public int getNegativeStrokeColor() {
        return mNegativeStrokeColor;
    }

    public void setNegativeStrokeColor(int negativeStrokeColor) {
        mNegativeStrokeColor = negativeStrokeColor;
    }

    public Shader getNegativeShader() {
        return mNegativeShader;
    }

    public void setNegativeShader(Shader negativeShader) {
        mNegativeShader = negativeShader;
    }

}
