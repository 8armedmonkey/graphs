package com.eightam.android.graphs.layout;

import com.eightam.android.graphs.point.GraphPoint;

import java.util.List;

public class BubbleLayoutManager implements GraphLayoutManager {

    private static final float ANGLE = 45.0f;

    private float mMaxBubbleRadius;
    private float mMinBubbleRadius;
    private float mGap;

    private float mRelativePaddingLeft;
    private float mRelativePaddingTop;
    private float mRelativePaddingRight;
    private float mRelativePaddingBottom;

    public BubbleLayoutManager(float maxBubbleRadius, float minBubbleRadius, float gap) {
        this(maxBubbleRadius, minBubbleRadius, gap, 0.0f, 0.0f, 0.0f, 0.0f);
    }

    public BubbleLayoutManager(float maxBubbleRadius, float minBubbleRadius, float gap,
                               float relativePaddingLeft, float relativePaddingTop, float relativePaddingRight, float relativePaddingBottom) {
        mMaxBubbleRadius = maxBubbleRadius;
        mMinBubbleRadius = minBubbleRadius;
        mGap = gap;
        mRelativePaddingLeft = relativePaddingLeft;
        mRelativePaddingTop = relativePaddingTop;
        mRelativePaddingRight = relativePaddingRight;
        mRelativePaddingBottom = relativePaddingBottom;
    }

    @Override
    public List<? extends GraphPoint> layout(List<? extends GraphPoint> points) {
        int numOfPoints = points.size();

        if (numOfPoints > 0) {
            float maxAbsValue = Math.abs(points.get(0).getZ());
            float minAbsValue = Math.abs(points.get(0).getZ());
            float sumAbsValue = 0.0f;

            for (int i = 0; i < numOfPoints; i++) {
                GraphPoint point = points.get(i);
                float absValue = Math.abs(point.getZ());

                maxAbsValue = Math.max(maxAbsValue, absValue);
                minAbsValue = Math.min(minAbsValue, absValue);

                sumAbsValue = sumAbsValue + absValue;
            }

            boolean noDifference = Float.compare(sumAbsValue, 0.0f) == 0;

            if (noDifference) {
                sumAbsValue = 1.0f;
            }

            float w = 1.0f - (mRelativePaddingLeft + mRelativePaddingRight);
            float h = 1.0f - (mRelativePaddingTop + mRelativePaddingBottom);
            float sw = Math.min(w, h);

            float x0 = mRelativePaddingLeft;
            float y0 = mRelativePaddingTop;

            float cx = x0 + 0.5f * w;
            float cy = y0 + 0.5f * h;

            GraphPoint previousPoint = points.get(0);
            float previousRadius = limitRadius(0.5f * Math.abs(previousPoint.getZ()) / sumAbsValue * sw);

            previousPoint.setX(x0 + previousRadius);
            previousPoint.setY(y0 + previousRadius);

            float minX = previousPoint.getX() - previousRadius;
            float minY = previousPoint.getY() - previousRadius;
            float maxX = previousPoint.getX() + previousRadius;
            float maxY = previousPoint.getY() + previousRadius;

            for (int i = 1; i < numOfPoints; i++) {
                GraphPoint point = points.get(i);
                float radius = limitRadius(0.5f * Math.abs(point.getZ()) / sumAbsValue * sw);
                float angle = noDifference ? 0.0f : (point.getZ() > 0 ? -ANGLE : ANGLE);
                float distance = previousRadius + radius + mGap;

                point.setX(previousPoint.getX() + distance * (float) Math.cos(Math.toRadians(angle)));
                point.setY(previousPoint.getY() + distance * (float) Math.sin(Math.toRadians(angle)));

                previousPoint = point;
                previousRadius = radius;

                minX = Math.min(minX, point.getX() - radius);
                minY = Math.min(minY, point.getY() - radius);
                maxX = Math.max(maxX, point.getX() + radius);
                maxY = Math.max(maxY, point.getY() + radius);
            }

            float tx = cx - 0.5f * (minX + maxX);
            float ty = cy - 0.5f * (minY + maxY);

            for (int i = 0; i < numOfPoints; i++) {
                GraphPoint point = points.get(i);
                point.setX(point.getX() + tx);
                point.setY(point.getY() + ty);
            }
        }
        return points;
    }

    @Override
    public float getRelativePaddingLeft() {
        return mRelativePaddingLeft;
    }

    @Override
    public float getRelativePaddingTop() {
        return mRelativePaddingTop;
    }

    @Override
    public float getRelativePaddingRight() {
        return mRelativePaddingRight;
    }

    @Override
    public float getRelativePaddingBottom() {
        return mRelativePaddingBottom;
    }

    private float limitRadius(float radius) {
        return Math.max(mMinBubbleRadius, Math.min(mMaxBubbleRadius, radius));
    }

}
