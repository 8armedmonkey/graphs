package com.eightam.android.graphs.layout;

import com.eightam.android.graphs.point.GraphPoint;

import java.util.List;

public class CenterVerticalLayoutManager implements GraphLayoutManager {

    private float mRelativePaddingLeft;
    private float mRelativePaddingTop;
    private float mRelativePaddingRight;
    private float mRelativePaddingBottom;

    public CenterVerticalLayoutManager() {
        this(0.0f, 0.0f, 0.0f, 0.0f);
    }

    public CenterVerticalLayoutManager(float relativePaddingLeft, float relativePaddingTop, float relativePaddingRight, float relativePaddingBottom) {
        mRelativePaddingLeft = relativePaddingLeft;
        mRelativePaddingTop = relativePaddingTop;
        mRelativePaddingRight = relativePaddingRight;
        mRelativePaddingBottom = relativePaddingBottom;
    }

    @Override
    public List<? extends GraphPoint> layout(List<? extends GraphPoint> points) {
        int numOfPoints = points.size();

        float w = 1.0f - (mRelativePaddingLeft + mRelativePaddingRight);
        float h = 1.0f - (mRelativePaddingTop + mRelativePaddingBottom);

        float dx = w / (numOfPoints - 1);
        float x0 = mRelativePaddingLeft;
        float y0 = mRelativePaddingTop;

        for (int i = 0; i < numOfPoints; i++) {
            GraphPoint point = points.get(i);
            point.setX(x0 + i * dx);
            point.setY(y0 + 0.5f * h);
        }
        return points;
    }

    @Override
    public float getRelativePaddingLeft() {
        return mRelativePaddingLeft;
    }

    @Override
    public float getRelativePaddingTop() {
        return mRelativePaddingTop;
    }

    @Override
    public float getRelativePaddingRight() {
        return mRelativePaddingRight;
    }

    @Override
    public float getRelativePaddingBottom() {
        return mRelativePaddingBottom;
    }

}