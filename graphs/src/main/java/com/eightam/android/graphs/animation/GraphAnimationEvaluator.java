package com.eightam.android.graphs.animation;

import android.animation.FloatEvaluator;
import android.support.annotation.NonNull;

import com.eightam.android.graphs.widget.GraphView;

public abstract class GraphAnimationEvaluator extends FloatEvaluator {

    private GraphView mGraphView;

    public GraphAnimationEvaluator(GraphView graphView) {
        mGraphView = graphView;
    }

    @NonNull
    @Override
    public Float evaluate(float fraction, Number startValue, Number endValue) {
        Float value = super.evaluate(fraction, startValue, endValue);

        useValue(value);
        invalidateGraphView();

        return value;
    }

    protected void invalidateGraphView() {
        if (mGraphView != null) {
            mGraphView.postInvalidate();
        }
    }

    protected abstract void useValue(float value);

}
