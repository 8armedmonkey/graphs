package com.eightam.android.graphs.behavior;

public interface Scalable {

    float getScaleX();

    void setScaleX(float scaleX);

    float getScaleY();

    void setScaleY(float scaleY);

}
