package com.eightam.android.graphs.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.animation.OvershootInterpolator;

import com.eightam.android.graphs.animation.ScaleXYEvaluator;
import com.eightam.android.graphs.layout.EquilateralTriangleLayoutManager;
import com.eightam.android.graphs.layout.GraphLayoutManager;
import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.point.ScalableGraphPoint;
import com.eightam.android.graphs.renderer.EquilateralTriangleRenderer;
import com.eightam.android.graphs.renderer.GraphPointRenderer;

import java.util.ArrayList;
import java.util.List;

public class ModernGraphView extends SimpleGraphView<ScalableGraphPoint> {

    private static final long ANIMATION_START_DELAY = 200;
    private static final float OVERSHOOT_TENSION = 2.0f;

    private Animator mAnimator;

    public ModernGraphView(Context context) {
        super(context);
    }

    public ModernGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ModernGraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void startAnimation() {
        synchronized (ModernGraphView.this) {
            stopAnimation();

            if (mPoints != null) {
                mAnimator = new AnimatorSet();
                List<Animator> animators = new ArrayList<>();

                for (int i = 0, n = mPoints.size(); i < n; i++) {
                    ScalableGraphPoint point = mPoints.get(i);
                    point.setScaleX(0.0f);
                    point.setScaleY(0.0f);

                    Animator animator = ValueAnimator.ofObject(new ScaleXYEvaluator(ModernGraphView.this, point), 0.0f, 1.0f);

                    if (mAnimationDurationMillis > 0) {
                        animator.setInterpolator(new OvershootInterpolator(OVERSHOOT_TENSION));
                        animator.setStartDelay(i * ANIMATION_START_DELAY);
                    }

                    animator.setDuration(mAnimationDurationMillis);

                    animators.add(animator);
                }

                ((AnimatorSet) mAnimator).playTogether(animators);
                mAnimator.start();
            }
        }
    }

    @Override
    public void stopAnimation() {
        synchronized (ModernGraphView.this) {
            if (mAnimator != null) {
                mAnimator.end();
            }
        }
    }

    @Override
    public void setPoints(List<? extends GraphPoint> points) {
        super.setPoints(points);
        setTriangleSideModifierForRenderer();
    }

    @Override
    public void setLayoutManager(GraphLayoutManager layoutManager) {
        if (!(layoutManager instanceof EquilateralTriangleLayoutManager)) {
            throw new IllegalArgumentException("Incompatible layout manager: "
                    + ModernGraphView.class.getName() + " requires " + EquilateralTriangleLayoutManager.class.getName());
        }
        super.setLayoutManager(layoutManager);
    }

    @Override
    public void setPointRenderer(GraphPointRenderer<ScalableGraphPoint> renderer) {
        if (!(renderer instanceof EquilateralTriangleRenderer)) {
            throw new IllegalArgumentException("Incompatible point renderer: "
                    + ModernGraphView.class.getName() + " requires " + EquilateralTriangleRenderer.class.getName());
        }
        super.setPointRenderer(renderer);
        setTriangleSideModifierForRenderer();
    }

    @Override
    protected void setPointsInternal(List<? extends GraphPoint> points) {
        List<ScalableGraphPoint> internalPoints = new ArrayList<>();

        for (GraphPoint point : points) {
            internalPoints.add(new ScalableGraphPoint(point, 0.0f, 0.0f));
        }

        mPoints = internalPoints;
    }

    @Override
    protected void onPreRenderGraphPoints(Canvas canvas) {
        super.onPreRenderGraphPoints(canvas);

        int shortestWidth = Math.min(getWidth(), getHeight());

        canvas.rotate(-EquilateralTriangleLayoutManager.LAYOUT_ROTATION, 0.5f * shortestWidth, 0.5f * shortestWidth);
    }

    private void setTriangleSideModifierForRenderer() {
        if (mPoints != null && mPointRenderer != null) {
            float sumAbsValue = 0.0f;

            for (GraphPoint point : mPoints) {
                sumAbsValue = sumAbsValue + Math.abs(point.getZ());
            }

            boolean noDifference = Float.compare(sumAbsValue, 0.0f) == 0;

            if (noDifference) {
                sumAbsValue = 1.0f;
            }

            float triangleSideModifier = 1.0f / sumAbsValue;

            ((EquilateralTriangleRenderer) mPointRenderer).setTriangleSideModifier(triangleSideModifier);
        }
    }

    public static class Builder extends GraphView.Builder<Builder, ScalableGraphPoint> {

        public Builder(Context context) {
            super(context);
        }

        @Override
        void createGraphView(Context context) {
            mGraphView = new ModernGraphView(context);
        }

    }

}
