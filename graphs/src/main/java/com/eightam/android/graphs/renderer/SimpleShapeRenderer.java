package com.eightam.android.graphs.renderer;

import android.graphics.Paint;
import android.graphics.Shader;

import com.eightam.android.graphs.point.GraphPoint;

public abstract class SimpleShapeRenderer<T extends GraphPoint> implements GraphPointRenderer<T> {

    protected Paint mPaint;
    protected Shader mShader;
    protected int mFillColor;
    protected int mStrokeColor;
    protected boolean mShouldFill;
    protected boolean mShouldStroke;

    public SimpleShapeRenderer() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStyle(Paint.Style.FILL);

        mShouldFill = true;
        mShouldStroke = false;
    }

    public int getFillColor() {
        return mFillColor;
    }

    public void setFillColor(int fillColor) {
        mFillColor = fillColor;
    }

    public int getStrokeColor() {
        return mStrokeColor;
    }

    public void setStrokeColor(int strokeColor) {
        mStrokeColor = strokeColor;
    }

    public boolean shouldFill() {
        return mShouldFill;
    }

    public boolean shouldStroke() {
        return mShouldStroke;
    }

    public void setShouldFill(boolean shouldFill) {
        mShouldFill = shouldFill;
    }

    public void setShouldStroke(boolean shouldStroke) {
        mShouldStroke = shouldStroke;
    }

    public float getStrokeWidth() {
        return mPaint.getStrokeWidth();
    }

    public void setStrokeWidth(float strokeWidth) {
        mPaint.setStrokeWidth(strokeWidth);
    }

    public Shader getShader() {
        return mShader;
    }

    public void setShader(Shader shader) {
        mShader = shader;
    }

}
