package com.eightam.android.graphs.renderer;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;

import com.eightam.android.graphs.point.ScalableGraphPoint;

public class BubbleRenderer extends SimpleShapeRenderer<ScalableGraphPoint> {

    private float mMaxRelativeBubbleRadius;
    private float mMinRelativeBubbleRadius;
    private float mBubbleRadiusModifier;
    private float mRelativePaddingLeft;
    private float mRelativePaddingTop;
    private float mRelativePaddingRight;
    private float mRelativePaddingBottom;
    private RectF mOvalRect;

    private int mPositiveFillColor;
    private int mPositiveStrokeColor;
    private Shader mPositiveShader;

    private int mNegativeFillColor;
    private int mNegativeStrokeColor;
    private Shader mNegativeShader;

    private Matrix mShaderMatrix;

    public BubbleRenderer(float maxRelativeBubbleRadius, float minRelativeBubbleRadius, float bubbleRadiusModifier,
                          float relativePaddingLeft, float relativePaddingTop, float relativePaddingRight, float relativePaddingBottom) {
        mMaxRelativeBubbleRadius = maxRelativeBubbleRadius;
        mMinRelativeBubbleRadius = minRelativeBubbleRadius;
        mBubbleRadiusModifier = bubbleRadiusModifier;
        mRelativePaddingLeft = relativePaddingLeft;
        mRelativePaddingTop = relativePaddingTop;
        mRelativePaddingRight = relativePaddingRight;
        mRelativePaddingBottom = relativePaddingBottom;
        mOvalRect = new RectF();
        mShaderMatrix = new Matrix();
    }

    @Override
    public void render(Canvas canvas, int containerWidth, int containerHeight, ScalableGraphPoint graphPoint) {
        canvas.save();

        int shortestWidth = Math.min(containerWidth, containerHeight);

        int shortestWidthWithoutPadding = (int) Math.min(
                (1.0f - (mRelativePaddingLeft + mRelativePaddingRight)) * containerWidth,
                (1.0f - (mRelativePaddingTop + mRelativePaddingBottom)) * containerHeight);

        float x = graphPoint.getX() * shortestWidth;
        float y = graphPoint.getY() * shortestWidth;
        float z = graphPoint.getZ();

        float maxR = mMaxRelativeBubbleRadius * shortestWidthWithoutPadding;
        float minR = mMinRelativeBubbleRadius * shortestWidthWithoutPadding;
        float r = Math.min(maxR, Math.max(minR, 0.5f * Math.abs(z) * mBubbleRadiusModifier * shortestWidthWithoutPadding));

        mOvalRect.set(x - r, y - r, x + r, y + r);

        Shader shader = Float.compare(z, 0.0f) > 0 ? mPositiveShader : mNegativeShader;

        if (shader != null) {
            shader.getLocalMatrix(mShaderMatrix);

            mShaderMatrix.reset();
            mShaderMatrix.postScale(r, 1.0f);
            mShaderMatrix.postTranslate(x - r, 0.0f);

            shader.setLocalMatrix(mShaderMatrix);
        }

        if (mShouldFill) {
            int fillColor = Float.compare(z, 0.0f) > 0 ? mPositiveFillColor : mNegativeFillColor;

            mPaint.setStyle(Paint.Style.FILL);

            if (shader != null) {
                mPaint.setShader(shader);
            } else {
                mPaint.setColor(fillColor);
            }

            canvas.save();
            canvas.scale(graphPoint.getScaleX(), graphPoint.getScaleY(), x, y);
            canvas.drawOval(mOvalRect, mPaint);
            canvas.restore();
        }

        if (mShouldStroke) {
            int strokeColor = Float.compare(z, 0.0f) > 0 ? mPositiveStrokeColor : mNegativeStrokeColor;

            mPaint.setStyle(Paint.Style.STROKE);

            if (shader != null) {
                mPaint.setShader(shader);
            } else {
                mPaint.setColor(strokeColor);
            }

            canvas.save();
            canvas.scale(graphPoint.getScaleX(), graphPoint.getScaleY(), x, y);
            canvas.drawOval(mOvalRect, mPaint);
            canvas.restore();
        }

        canvas.restore();
    }

    public float getBubbleRadiusModifier() {
        return mBubbleRadiusModifier;
    }

    public void setBubbleRadiusModifier(float bubbleRadiusModifier) {
        mBubbleRadiusModifier = bubbleRadiusModifier;
    }

    public int getPositiveFillColor() {
        return mPositiveFillColor;
    }

    public void setPositiveFillColor(int positiveFillColor) {
        mPositiveFillColor = positiveFillColor;
    }

    public int getPositiveStrokeColor() {
        return mPositiveStrokeColor;
    }

    public void setPositiveStrokeColor(int positiveStrokeColor) {
        mPositiveStrokeColor = positiveStrokeColor;
    }

    public Shader getPositiveShader() {
        return mPositiveShader;
    }

    public void setPositiveShader(Shader positiveShader) {
        mPositiveShader = positiveShader;
    }

    public int getNegativeFillColor() {
        return mNegativeFillColor;
    }

    public void setNegativeFillColor(int negativeFillColor) {
        mNegativeFillColor = negativeFillColor;
    }

    public int getNegativeStrokeColor() {
        return mNegativeStrokeColor;
    }

    public void setNegativeStrokeColor(int negativeStrokeColor) {
        mNegativeStrokeColor = negativeStrokeColor;
    }

    public Shader getNegativeShader() {
        return mNegativeShader;
    }

    public void setNegativeShader(Shader negativeShader) {
        mNegativeShader = negativeShader;
    }

}
