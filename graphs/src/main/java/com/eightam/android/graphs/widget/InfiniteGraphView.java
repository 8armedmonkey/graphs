package com.eightam.android.graphs.widget;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.eightam.android.graphs.animation.RotateEvaluator;
import com.eightam.android.graphs.layout.GraphLayoutManager;
import com.eightam.android.graphs.layout.SpiralArcLayoutManager;
import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.point.IndexableGraphPoint;
import com.eightam.android.graphs.renderer.GraphPointRenderer;
import com.eightam.android.graphs.renderer.SpiralArcRenderer;

import java.util.ArrayList;
import java.util.List;

public class InfiniteGraphView extends SimpleGraphView<IndexableGraphPoint> {

    private com.eightam.android.graphs.behavior.Rotatable mRotatable;
    private Animator mAnimator;

    public InfiniteGraphView(Context context) {
        super(context);
        initialize();
    }

    public InfiniteGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public InfiniteGraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @Override
    public void startAnimation() {
        synchronized (InfiniteGraphView.this) {
            stopAnimation();

            mAnimator = ValueAnimator.ofObject(new RotateEvaluator(InfiniteGraphView.this, mRotatable), 0.0f, 360.0f);
            mAnimator.setDuration(mAnimationDurationMillis);
            mAnimator.start();
        }
    }

    @Override
    public void stopAnimation() {
        synchronized (InfiniteGraphView.this) {
            if (mAnimator != null) {
                mAnimator.end();
            }
        }
    }

    @Override
    public void setPoints(List<? extends GraphPoint> points) {
        super.setPoints(points);
        setIntervalsForRenderer();
    }

    @Override
    public void setLayoutManager(GraphLayoutManager layoutManager) {
        if (!(layoutManager instanceof SpiralArcLayoutManager)) {
            throw new IllegalArgumentException("Incompatible layout manager: "
                    + InfiniteGraphView.class.getName() + " requires " + SpiralArcLayoutManager.class.getName());
        }
        super.setLayoutManager(layoutManager);
        setIntervalsForRenderer();
    }

    @Override
    public void setPointRenderer(GraphPointRenderer<IndexableGraphPoint> renderer) {
        if (!(renderer instanceof SpiralArcRenderer)) {
            throw new IllegalArgumentException("Incompatible point renderer: "
                    + InfiniteGraphView.class.getName() + " requires " + SpiralArcRenderer.class.getName());
        }
        super.setPointRenderer(renderer);
        setIntervalsForRenderer();
    }

    @Override
    protected void setPointsInternal(List<? extends GraphPoint> points) {
        List<IndexableGraphPoint> internalPoints = new ArrayList<>();
        int i = 0;

        for (GraphPoint point : points) {
            internalPoints.add(new IndexableGraphPoint(point, i++));
        }

        mPoints = internalPoints;
    }

    @Override
    protected void onPreRenderGraphPoints(Canvas canvas) {
        super.onPreRenderGraphPoints(canvas);

        float shortestWidth = Math.min(getWidth(), getHeight());

        canvas.scale(
                shortestWidth / SpiralArcRenderer.ARC_PATH_WIDTH,
                shortestWidth / SpiralArcRenderer.ARC_PATH_HEIGHT);

        canvas.rotate(mRotatable.getRotationAngle(),
                0.5f * SpiralArcRenderer.ARC_PATH_WIDTH,
                0.5f * SpiralArcRenderer.ARC_PATH_HEIGHT);
    }

    private void initialize() {
        mRotatable = new Rotatable(0.0f);
    }

    private void setIntervalsForRenderer() {
        if (mPoints != null && mLayoutManager != null && mPointRenderer != null) {
            ((SpiralArcRenderer) mPointRenderer).setIntervals(((SpiralArcLayoutManager) mLayoutManager).getIntervals());
        }
    }

    public static class Builder extends GraphView.Builder<Builder, IndexableGraphPoint> {

        public Builder(Context context) {
            super(context);
        }

        @Override
        void createGraphView(Context context) {
            mGraphView = new InfiniteGraphView(context);
        }

    }

    private static class Rotatable implements com.eightam.android.graphs.behavior.Rotatable {

        float mRotationAngle;

        public Rotatable(float rotationAngle) {
            mRotationAngle = rotationAngle;
        }

        @Override
        public float getRotationAngle() {
            return mRotationAngle;
        }

        @Override
        public void setRotationAngle(float rotationAngle) {
            mRotationAngle = rotationAngle;
        }
    }

}
