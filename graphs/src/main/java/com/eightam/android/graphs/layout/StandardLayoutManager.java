package com.eightam.android.graphs.layout;

import com.eightam.android.graphs.point.GraphPoint;

import java.util.List;

public class StandardLayoutManager implements GraphLayoutManager {

    private float mRelativePaddingLeft;
    private float mRelativePaddingTop;
    private float mRelativePaddingRight;
    private float mRelativePaddingBottom;

    public StandardLayoutManager() {
        this(0.0f, 0.0f, 0.0f, 0.0f);
    }

    public StandardLayoutManager(float relativePaddingLeft, float relativePaddingTop, float relativePaddingRight, float relativePaddingBottom) {
        mRelativePaddingLeft = relativePaddingLeft;
        mRelativePaddingTop = relativePaddingTop;
        mRelativePaddingRight = relativePaddingRight;
        mRelativePaddingBottom = relativePaddingBottom;
    }

    @Override
    public List<? extends GraphPoint> layout(List<? extends GraphPoint> points) {
        int numOfPoints = points.size();

        if (numOfPoints > 0) {
            float maxValue = points.get(0).getZ();
            float minValue = points.get(0).getZ();

            for (int i = 0; i < numOfPoints; i++) {
                GraphPoint point = points.get(i);

                maxValue = Math.max(maxValue, point.getZ());
                minValue = Math.min(minValue, point.getZ());
            }

            float valueSpan = Math.abs(maxValue - minValue);
            boolean noDifference = Float.compare(valueSpan, 0.0f) == 0;

            float w = 1.0f - (mRelativePaddingLeft + mRelativePaddingRight);
            float h = 1.0f - (mRelativePaddingTop + mRelativePaddingBottom);

            float dx = w / (numOfPoints - 1);
            float x0 = mRelativePaddingLeft;
            float y0 = mRelativePaddingTop;

            for (int i = 0; i < numOfPoints; i++) {
                GraphPoint point = points.get(i);
                point.setX(x0 + i * dx);

                if (noDifference) {
                    point.setY(y0 + h * 0.5f);
                } else {
                    point.setY(y0 + h * (1.0f - (point.getZ() - minValue) / valueSpan));
                }
            }
        }
        return points;
    }

    @Override
    public float getRelativePaddingLeft() {
        return mRelativePaddingLeft;
    }

    @Override
    public float getRelativePaddingTop() {
        return mRelativePaddingTop;
    }

    @Override
    public float getRelativePaddingRight() {
        return mRelativePaddingRight;
    }

    @Override
    public float getRelativePaddingBottom() {
        return mRelativePaddingBottom;
    }

}
