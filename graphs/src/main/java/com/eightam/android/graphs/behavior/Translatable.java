package com.eightam.android.graphs.behavior;

public interface Translatable {

    float getTranslationX();

    void setTranslationX(float translationX);

    float getTranslationY();

    void setTranslationY(float translationY);

}
