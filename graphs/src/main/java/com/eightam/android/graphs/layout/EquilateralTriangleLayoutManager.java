package com.eightam.android.graphs.layout;

import com.eightam.android.graphs.point.GraphPoint;

import java.util.List;

/**
 * Calculation for the bounding box:
 * http://stackoverflow.com/questions/9971230/calculate-rotated-rectangle-size-from-known-bounding-box-coordinates
 */
public class EquilateralTriangleLayoutManager implements GraphLayoutManager {

    public static final float LAYOUT_ROTATION = 30.0f;

    private static final float TRIANGLE_ANGLE = 60.0f;

    private float mMaxTriangleSide;
    private float mMinTriangleSide;
    private float mGap;

    public EquilateralTriangleLayoutManager(float maxTriangleSide, float minTriangleSide, float gap) {
        mMaxTriangleSide = maxTriangleSide;
        mMinTriangleSide = minTriangleSide;
        mGap = gap;
    }

    @Override
    public List<? extends GraphPoint> layout(List<? extends GraphPoint> points) {
        int numOfPoints = points.size();

        if (numOfPoints > 0) {
            float maxAbsValue = Math.abs(points.get(0).getZ());
            float minAbsValue = Math.abs(points.get(0).getZ());
            float sumAbsValue = 0.0f;

            for (int i = 0; i < numOfPoints; i++) {
                GraphPoint point = points.get(i);
                float absValue = Math.abs(point.getZ());

                maxAbsValue = Math.max(maxAbsValue, absValue);
                minAbsValue = Math.min(minAbsValue, absValue);

                sumAbsValue = sumAbsValue + absValue;
            }

            boolean noDifference = Float.compare(sumAbsValue, 0.0f) == 0;

            if (noDifference) {
                sumAbsValue = 1.0f;
            }

            float w = 1.0f;
            float h = 1.0f;

            float cosA = (float) Math.cos(Math.toRadians(LAYOUT_ROTATION));
            float sinA = (float) Math.sin(Math.toRadians(LAYOUT_ROTATION));

            float aw = (1.0f / (cosA * cosA - sinA * sinA)) * ( w * cosA - h * sinA);
            float ah = (1.0f / (cosA * cosA - sinA * sinA)) * (-w * sinA + h * cosA);

            float x0 = 0.5f * (w - aw);
            float y0 = 0.5f * (h - ah);

            Triangle[] triangles = new Triangle[numOfPoints];
            float spanX, spanY, minY, maxY;

            spanX = 0.0f;
            minY = 0.0f;
            maxY = 0.0f;

            for (int i = 0; i < numOfPoints; i++) {
                GraphPoint point = points.get(i);
                float side = limitSide(Math.abs(point.getZ()) / sumAbsValue * aw);

                triangles[i] = new Triangle(side, calculateTriangleHeightBySide(side), calculateTriangleRadiusBySide(side));

                spanX = spanX + side + (i < numOfPoints - 1 ? mGap : 0);
                minY = Math.min(minY, point.getZ() > 0 ? triangles[i].mHeight : -triangles[i].mHeight);
                maxY = Math.max(maxY, point.getZ() > 0 ? triangles[i].mHeight : -triangles[i].mHeight);
            }

            spanY = maxY - minY;

            float scaledWidth = spanX;
            float scaledHeight = spanY;

            boolean scaleBySide;
            float scale;

            if (scaledWidth > scaledHeight) {
                scaledWidth = aw;
                scaledHeight = spanY / spanX * scaledWidth;

                scaleBySide = true;
                scale = scaledWidth / spanX;

                if (scaledHeight > ah) {
                    scaledHeight = ah;

                    scaleBySide = false;
                    scale = scaledHeight / spanY;
                }
            } else {
                scaledHeight = ah;
                scaledWidth = spanX / spanY * scaledHeight;

                scaleBySide = false;
                scale = scaledHeight / spanY;

                if (scaledWidth > aw) {
                    scaledWidth = aw;

                    scaleBySide = true;
                    scale = scaledWidth / spanX;
                }
            }

            GraphPoint previousPoint = points.get(0);
            Triangle previousTriangle = triangles[0];

            scaleTriangle(previousTriangle, scaleBySide, scale);

            previousPoint.setX(x0 + 0.5f * previousTriangle.mSide);
            previousPoint.setY(y0 + 0.5f * ah);

            for (int i = 1; i < numOfPoints; i++) {
                GraphPoint point = points.get(i);
                Triangle triangle = triangles[i];

                scaleTriangle(triangle, scaleBySide, scale);

                float distance = 0.5f * previousTriangle.mSide + 0.5f * triangle.mSide + mGap;

                point.setX(previousPoint.getX() + distance);
                point.setY(y0 + 0.5f * ah);

                previousPoint = point;
                previousTriangle = triangle;
            }
        }
        return points;
    }

    @Override
    public float getRelativePaddingLeft() {
        return 0;
    }

    @Override
    public float getRelativePaddingTop() {
        return 0;
    }

    @Override
    public float getRelativePaddingRight() {
        return 0;
    }

    @Override
    public float getRelativePaddingBottom() {
        return 0;
    }

    private float limitSide(float side) {
        return Math.max(mMinTriangleSide, Math.min(mMaxTriangleSide, side));
    }

    private float calculateTriangleHeightBySide(float side) {
        return (float) Math.abs(side * Math.sin(Math.toRadians(TRIANGLE_ANGLE)));
    }

    private float calculateTriangleRadiusBySide(float side) {
        return (float) Math.abs(side / (2 * Math.sin(Math.toDegrees(TRIANGLE_ANGLE))));
    }

    private float calculateTriangleSideByHeight(float height) {
        return (float) Math.abs(height / Math.sin(Math.toRadians(TRIANGLE_ANGLE)));
    }

    private void scaleTriangle(Triangle triangle, boolean scaleBySide, float scale) {
        if (scaleBySide) {
            triangle.mSide = triangle.mSide * scale;
            triangle.mHeight = calculateTriangleSideByHeight(triangle.mSide);
            triangle.mRadius = calculateTriangleRadiusBySide(triangle.mSide);
        } else {
            triangle.mHeight = triangle.mHeight * scale;
            triangle.mSide = calculateTriangleSideByHeight(triangle.mHeight);
            triangle.mRadius = calculateTriangleRadiusBySide(triangle.mSide);
        }
    }

    private static class Triangle {

        float mSide;
        float mHeight;
        float mRadius;

        public Triangle(float side, float height, float radius) {
            mSide = side;
            mHeight = height;
            mRadius = radius;
        }

    }

}