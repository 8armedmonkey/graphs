package com.eightam.android.graphs.point;

import com.eightam.android.graphs.behavior.Indexable;

public class IndexableGraphPoint extends GraphPoint implements Indexable {

    private int mIndex;

    public IndexableGraphPoint(int index) {
        this(0.0f, 0.0f, 0.0f, index);
    }

    public IndexableGraphPoint(float x, float y, float z, int index) {
        super(x, y, z);
        mIndex = index;
    }

    public IndexableGraphPoint(GraphPoint point, int index) {
        this(point.mX, point.mY, point.mZ, index);
    }

    @Override
    public int getIndex() {
        return mIndex;
    }

}
