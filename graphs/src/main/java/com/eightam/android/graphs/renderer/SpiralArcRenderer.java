package com.eightam.android.graphs.renderer;

import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;

import com.eightam.android.graphs.point.IndexableGraphPoint;

import java.util.ArrayList;
import java.util.List;

public class SpiralArcRenderer extends SimpleShapeRenderer<IndexableGraphPoint> {

    public static final float ARC_TOTAL_LENGTH = 4384.0f;
    public static final float ARC_PATH_WIDTH = 750.0f;
    public static final float ARC_PATH_HEIGHT = 750.0f;

    private float[][] mIntervals;
    private Path mPath;
    private List<PathEffect> mPathEffects;
    private int mPositiveArcColor;
    private int mNegativeArcColor;

    public SpiralArcRenderer(float[][] intervals, int positiveArcColor, int negativeArcColor) {
        mIntervals = intervals;
        mPositiveArcColor = positiveArcColor;
        mNegativeArcColor = negativeArcColor;

        initializePath();
        initializePathEffects();
    }

    @Override
    public void render(Canvas canvas, int containerWidth, int containerHeight, IndexableGraphPoint graphPoint) {
        canvas.save();

        float z = graphPoint.getZ();

        mPaint.setColor(Float.compare(z, 0.0f) > 0 ? mPositiveArcColor : mNegativeArcColor);
        mPaint.setStyle(Paint.Style.STROKE);

        PathEffect pathEffect = getPathEffect(graphPoint.getIndex());

        if (pathEffect != null) {
            mPaint.setPathEffect(pathEffect);
        }

        canvas.drawPath(mPath, mPaint);

        canvas.restore();
    }

    public float[][] getIntervals() {
        return mIntervals;
    }

    public void setIntervals(float[][] intervals) {
        mIntervals = intervals;
    }

    private void initializePath() {
        mPath = new Path();

        mPath.moveTo(484.93f, 409.24f);
        mPath.cubicTo(484.93f, 457.77f, 445.62f, 497.11f, 397.13f, 497.11f);
        mPath.cubicTo(343.78f, 497.11f, 300.54f, 453.84f, 300.54f, 400.46f);
        mPath.cubicTo(300.54f, 341.74f, 348.1f, 294.13f, 406.78f, 294.13f);
        mPath.cubicTo(471.33f, 294.13f, 523.66f, 346.5f, 523.66f, 411.09f);
        mPath.cubicTo(523.66f, 482.14f, 466.1f, 539.73f, 395.1f, 539.73f);
        mPath.cubicTo(316.99f, 539.73f, 253.68f, 476.38f, 253.68f, 398.22f);
        mPath.cubicTo(253.68f, 312.25f, 323.33f, 242.56f, 409.24f, 242.56f);
        mPath.cubicTo(503.74f, 242.56f, 580.35f, 319.22f, 580.35f, 413.79f);
        mPath.cubicTo(580.35f, 517.81f, 496.08f, 602.14f, 392.13f, 602.14f);
        mPath.cubicTo(277.78f, 602.14f, 185.08f, 509.38f, 185.08f, 394.95f);
        mPath.cubicTo(185.08f, 269.08f, 287.05f, 167.05f, 412.83f, 167.05f);
        mPath.cubicTo(551.19f, 167.05f, 663.36f, 279.29f, 663.36f, 417.74f);
        mPath.cubicTo(663.36f, 570.05f, 539.98f, 693.51f, 387.78f, 693.51f);
        mPath.cubicTo(220.36f, 693.51f, 84.64f, 557.7f, 84.64f, 390.17f);
        mPath.cubicTo(84.64f, 205.88f, 233.93f, 56.49f, 418.09f, 56.49f);
    }

    private void initializePathEffects() {
        mPathEffects = new ArrayList<>();

        for (int i = 0, n = mIntervals.length; i < n; i++) {
            mPathEffects.add(new DashPathEffect(mIntervals[i], 0.0f));
        }
    }

    private PathEffect getPathEffect(int index) {
        if (index >= 0 && index < mPathEffects.size()) {
            return mPathEffects.get(index);
        }
        return null;
    }

}
