package com.eightam.android.graphs.renderer;

import android.graphics.Canvas;

import com.eightam.android.graphs.point.GraphPoint;

public class NothingRenderer<T extends GraphPoint> implements GraphPointRenderer<T> {

    @Override
    public void render(Canvas canvas, int containerWidth, int containerHeight, T graphPoint) {
    }

}
