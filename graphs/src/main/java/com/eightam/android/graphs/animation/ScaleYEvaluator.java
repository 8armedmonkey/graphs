package com.eightam.android.graphs.animation;

import com.eightam.android.graphs.behavior.Scalable;
import com.eightam.android.graphs.widget.GraphView;

public class ScaleYEvaluator extends GraphAnimationEvaluator {

    private Scalable mScalable;

    public ScaleYEvaluator(GraphView graphView, Scalable scalable) {
        super(graphView);
        mScalable = scalable;
    }

    @Override
    protected void useValue(float value) {
        mScalable.setScaleY(value);
    }

}
