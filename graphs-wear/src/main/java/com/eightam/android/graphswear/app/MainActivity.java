package com.eightam.android.graphswear.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.wearable.view.WatchViewStub;

import com.eightam.android.graphswear.R;
import com.eightam.android.graphswear.fragment.GraphGalleryFragment;

public class MainActivity extends AppCompatActivity implements WatchViewStub.OnLayoutInflatedListener {

    private static final String FRAGMENT_GRAPH_GALLERY = "graphGallery";

    @Override
    public void onLayoutInflated(WatchViewStub watchViewStub) {
        showGraphGallery();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(MainActivity.this);
    }

    private void showGraphGallery() {
        GraphGalleryFragment graphGalleryFragment =
                (GraphGalleryFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_GRAPH_GALLERY);

        if (graphGalleryFragment == null) {
            graphGalleryFragment = GraphGalleryFragment.newInstance();

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, graphGalleryFragment, FRAGMENT_GRAPH_GALLERY)
                    .commit();
        }
    }

}
