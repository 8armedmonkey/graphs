package com.eightam.android.graphswear.widget;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.eightam.android.graphs.model.GraphType;
import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.widget.GraphView;
import com.eightam.android.graphs.widget.GraphViewFactory;

import java.util.ArrayList;
import java.util.List;

public class GraphGalleryAdapter extends PagerAdapter {

    private Context mContext;
    private GraphType[] mGraphTypes = new GraphType[] {
            GraphType.BUBBLES, GraphType.MODERN, GraphType.SOUND, GraphType.CLASSIC, GraphType.WAVE, GraphType.INFINITE
    };

    public GraphGalleryAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return mGraphTypes.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = createDemoGraphView(mGraphTypes[position]);

        view.setBackgroundColor(mContext.getResources().getColor(android.R.color.black));

        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        container.addView(view, params);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    private List<GraphPoint> createDemoPoints() {
        List<GraphPoint> points = new ArrayList<>();
        points.add(new GraphPoint(10.0f));
        points.add(new GraphPoint(-20.0f));
        points.add(new GraphPoint(-30.0f));
        points.add(new GraphPoint(20.0f));
        points.add(new GraphPoint(10.0f));

        return points;
    }

    private View createDemoGraphView(GraphType graphType) {
        View view = GraphViewFactory.createGraphView(mContext, graphType, createDemoPoints());
        view.setOnClickListener(new GraphListener((GraphView) view));

        return view;
    }

    private static class GraphListener implements View.OnClickListener {

        GraphView mGraphView;

        public GraphListener(GraphView graphView) {
            mGraphView = graphView;
        }

        @Override
        public void onClick(View v) {
            mGraphView.startAnimation();
        }
    }

}
