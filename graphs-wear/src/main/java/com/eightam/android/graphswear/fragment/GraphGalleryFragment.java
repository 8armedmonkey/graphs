package com.eightam.android.graphswear.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eightam.android.graphswear.R;
import com.eightam.android.graphswear.widget.GraphGalleryAdapter;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class GraphGalleryFragment extends Fragment {

    @InjectView(R.id.view_pager)
    ViewPager mViewPager;

    public static GraphGalleryFragment newInstance() {
        return new GraphGalleryFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_graph_gallery, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(GraphGalleryFragment.this, view);

        mViewPager.setAdapter(new GraphGalleryAdapter(getActivity()));
    }

}
